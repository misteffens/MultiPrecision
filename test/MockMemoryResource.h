//
// Copyright (C) 2021 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#ifndef MockMemoryResource_h_INCLUDED
#define MockMemoryResource_h_INCLUDED

#include <gmock/gmock.h>
#include <memory_resource>

class MockMemoryResource : public std::pmr::memory_resource
{
public:
	MockMemoryResource()
	{
		ON_CALL(*this, do_allocate).WillByDefault([this](std::size_t bytes, std::size_t alignment) {
			return upstreamResource->allocate(bytes, alignment);
		});
	}

	MOCK_METHOD(void*, do_allocate, (std::size_t bytes, std::size_t alignment), (override));

	void do_deallocate(void* p, std::size_t bytes, std::size_t alignment) override
	{
		upstreamResource->deallocate(p, bytes, alignment);
	}

	bool do_is_equal(std::pmr::memory_resource const& other) const noexcept override
	{
		return this == &other;
	}

private:
	std::pmr::memory_resource* upstreamResource{std::pmr::get_default_resource()};
};

#endif // MockMemoryResource_h_INCLUDED
