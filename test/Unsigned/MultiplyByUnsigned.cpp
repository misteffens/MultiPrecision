//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "MockMemoryResource.h"
#include "utils.h"
#include <MultiPrecision/Unsigned.h>
#include <gtest/gtest.h>

namespace MultiPrecision {

TEST(MultiplyByUnsigned, Zero)
{
	{
		Unsigned testee{u({})};
		EXPECT_EQ(&(testee *= u({})), &testee);
		EXPECT_EQ(testee, u({}));
	}
	{
		Unsigned testee{u({1})};
		EXPECT_EQ(&(testee *= u({})), &testee);
		EXPECT_EQ(testee, u({}));
	}
	{
		Unsigned testee{u({})};
		EXPECT_EQ(&(testee *= u({1})), &testee);
		EXPECT_EQ(testee, u({}));
	}
	{
		Unsigned testee{u({wMax, 1})};
		EXPECT_EQ(&(testee *= u({})), &testee);
		EXPECT_EQ(testee, u({}));
	}
	{
		Unsigned testee{u({})};
		EXPECT_EQ(&(testee *= u({wMax, 1})), &testee);
		EXPECT_EQ(testee, u({}));
	}
}

TEST(MultiplyByUnsigned, SmallWords)
{
	{
		Unsigned testee{u({1})};
		EXPECT_EQ(&(testee *= u({1})), &testee);
		EXPECT_EQ(testee, u({1}));
	}
	{
		Unsigned testee{u({1})};
		EXPECT_EQ(&(testee *= u({1, 1})), &testee);
		EXPECT_EQ(testee, u({1, 1}));
	}
	{
		Unsigned testee{u({1, 1})};
		EXPECT_EQ(&(testee *= u({1})), &testee);
		EXPECT_EQ(testee, u({1, 1}));
	}
	{
		Unsigned testee{u({1, 1})};
		EXPECT_EQ(&(testee *= u({1, 1, 1})), &testee);
		EXPECT_EQ(testee, u({1, 2, 2, 1}));
	}
	{
		Unsigned testee{u({1, 1, 1})};
		EXPECT_EQ(&(testee *= u({1, 1})), &testee);
		EXPECT_EQ(testee, u({1, 2, 2, 1}));
	}
	{
		Unsigned testee{u({1, 1, 1})};
		EXPECT_EQ(&(testee *= u({1, 1, 1})), &testee);
		EXPECT_EQ(testee, u({1, 2, 3, 2, 1}));
	}
}

TEST(MultiplyByUnsigned, LargeWords)
{
	{
		Unsigned testee{u({wMax})};
		EXPECT_EQ(&(testee *= u({wMax})), &testee);
		EXPECT_EQ(testee, u({1, wMax - 1}));
	}
	{
		Unsigned testee{u({wMax, wMax})};
		EXPECT_EQ(&(testee *= u({wMax})), &testee);
		EXPECT_EQ(testee, u({1, wMax, wMax - 1}));
	}
	{
		Unsigned testee{u({wMax})};
		EXPECT_EQ(&(testee *= u({wMax, wMax})), &testee);
		EXPECT_EQ(testee, u({1, wMax, wMax - 1}));
	}
	{
		Unsigned testee{u({wMax, wMax, wMax})};
		EXPECT_EQ(&(testee *= u({wMax, wMax})), &testee);
		EXPECT_EQ(testee, u({1, 0, wMax, wMax - 1, wMax}));
	}
	{
		Unsigned testee{u({wMax, wMax})};
		EXPECT_EQ(&(testee *= u({wMax, wMax, wMax})), &testee);
		EXPECT_EQ(testee, u({1, 0, wMax, wMax - 1, wMax}));
	}
	{
		Unsigned testee{u({wMax, wMax, wMax})};
		EXPECT_EQ(&(testee *= u({wMax, wMax, wMax})), &testee);
		EXPECT_EQ(testee, u({1, 0, 0, wMax - 1, wMax, wMax}));
	}
}

TEST(MultiplyByUnsigned, Allocations)
{
	{
		testing::StrictMock<MockMemoryResource> r1, r2;
		EXPECT_CALL(r1, do_allocate(testing::_, testing::_)).Times(0);
		EXPECT_CALL(r2, do_allocate(testing::_, testing::_)).Times(0);
		Unsigned testee{u({}, r1)};
		testee *= u({}, r2);
	}
	{
		testing::StrictMock<MockMemoryResource> r1, r2;
		EXPECT_CALL(r1, do_allocate(testing::_, testing::_)).Times(2);
		EXPECT_CALL(r2, do_allocate(testing::_, testing::_)).Times(0);
		Unsigned testee{u({wMax, 1}, r1)};
		testee *= u({}, r2);
	}
	{
		testing::StrictMock<MockMemoryResource> r1, r2;
		EXPECT_CALL(r1, do_allocate(testing::_, testing::_)).Times(2);
		EXPECT_CALL(r2, do_allocate(testing::_, testing::_)).Times(1);
		Unsigned testee{u({wMax, wMax}, r1)};
		testee *= u({wMax, wMax, wMax}, r2);
	}
}

TEST(MultiplyByUnsigned, OutOfMemory)
{
	{
		testing::StrictMock<MockMemoryResource> r;
		testing::Sequence s;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(1).InSequence(s);
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
		Unsigned testee{u({wMax, wMax}, r)};
		EXPECT_THROW(testee *= u({wMax, wMax, wMax}), std::bad_alloc);
	}
}

} // namespace MultiPrecision
