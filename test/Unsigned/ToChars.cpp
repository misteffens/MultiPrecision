//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "utils.h"
#include <MultiPrecision/Unsigned.h>
#include <MultiPrecision/charconv.h>
#include <gtest/gtest.h>

namespace MultiPrecision {

TEST(ToChars, InvalidBase)
{
	{
		std::array<char, 1> out;
		MultiPrecision::Unsigned testee{u({})};
		std::to_chars_result result{to_chars(out.data(), out.data() + out.size(), testee, 1)};
		EXPECT_EQ(result.ec, std::errc::invalid_argument);
	}
	{
		std::array<char, 1> out;
		MultiPrecision::Unsigned testee{u({})};
		std::to_chars_result result{to_chars(out.data(), out.data() + out.size(), testee, 37)};
		EXPECT_EQ(result.ec, std::errc::invalid_argument);
	}
}

TEST(ToChars, TooLarge)
{
	{
		std::array<char, 4> out;
		MultiPrecision::Unsigned testee{u({19})};
		std::to_chars_result result{to_chars(out.data(), out.data() + out.size(), testee, 2)};
		EXPECT_EQ(result.ec, std::errc::value_too_large);
	}
	{
		std::array<char, 96> out;
		MultiPrecision::Unsigned testee{(u({1}) << 500) + 1};
		std::to_chars_result result{to_chars(out.data(), out.data() + out.size(), testee, 36)};
		EXPECT_EQ(result.ec, std::errc::value_too_large);
	}
}

TEST(ToChars, Base2)
{
	{
		std::array<char, 1> out;
		MultiPrecision::Unsigned testee{u({})};
		std::to_chars_result result{to_chars(out.data(), out.data() + out.size(), testee, 2)};
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(std::string_view(out.data(), result.ptr - out.data()), std::string_view("0"));
	}
	{
		std::array<char, 1> out;
		MultiPrecision::Unsigned testee{u({1})};
		std::to_chars_result result{to_chars(out.data(), out.data() + out.size(), testee, 2)};
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(std::string_view(out.data(), result.ptr - out.data()), std::string_view("1"));
	}
	{
		std::array<char, 5> out;
		MultiPrecision::Unsigned testee{u({19})};
		std::to_chars_result result{to_chars(out.data(), out.data() + out.size(), testee, 2)};
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(std::string_view(out.data(), result.ptr - out.data()), std::string_view("10011"));
	}
}

TEST(ToChars, LargeBase2)
{
	std::array<char, 501> out;
	MultiPrecision::Unsigned testee{(u({1}) << 500) + 1};
	std::to_chars_result result{to_chars(out.data(), out.data() + out.size(), testee, 2)};
	EXPECT_EQ(result.ec, std::errc{});
	EXPECT_EQ(
		std::string_view(out.data(), result.ptr - out.data()),
		std::string_view(
			"100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
			"000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
			"000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
			"000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
			"000000000000000000000000000000000000000000000000000000001"));
}

TEST(ToChars, Base10)
{
	{
		std::array<char, 1> out;
		MultiPrecision::Unsigned testee{u({})};
		std::to_chars_result result{to_chars(out.data(), out.data() + out.size(), testee)};
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(std::string_view(out.data(), result.ptr - out.data()), std::string_view("0"));
	}
	{
		std::array<char, 1> out;
		MultiPrecision::Unsigned testee{u({9})};
		std::to_chars_result result{to_chars(out.data(), out.data() + out.size(), testee)};
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(std::string_view(out.data(), result.ptr - out.data()), std::string_view("9"));
	}
	{
		std::array<char, 2> out;
		MultiPrecision::Unsigned testee{u({19})};
		std::to_chars_result result{to_chars(out.data(), out.data() + out.size(), testee)};
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(std::string_view(out.data(), result.ptr - out.data()), std::string_view("19"));
	}
}

TEST(ToChars, LargeBase10)
{
	std::array<char, 151> out;
	MultiPrecision::Unsigned testee{(u({1}) << 500) + 1};
	std::to_chars_result result{to_chars(out.data(), out.data() + out.size(), testee)};
	EXPECT_EQ(result.ec, std::errc{});
	EXPECT_EQ(
		std::string_view(out.data(), result.ptr - out.data()),
		std::string_view(
			"327339060789614187001318969682759915221664204604306478948329136809613379640467455488327009232590415715088668412"
			"7560071009217256545885393053328527589377"));
}

TEST(ToChars, Base11)
{
	{
		std::array<char, 1> out;
		MultiPrecision::Unsigned testee{u({})};
		std::to_chars_result result{to_chars(out.data(), out.data() + out.size(), testee, 11)};
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(std::string_view(out.data(), result.ptr - out.data()), std::string_view("0"));
	}
	{
		std::array<char, 1> out;
		MultiPrecision::Unsigned testee{u({10})};
		std::to_chars_result result{to_chars(out.data(), out.data() + out.size(), testee, 11)};
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(std::string_view(out.data(), result.ptr - out.data()), std::string_view("a"));
	}
	{
		std::array<char, 2> out;
		MultiPrecision::Unsigned testee{u({21})};
		std::to_chars_result result{to_chars(out.data(), out.data() + out.size(), testee, 11)};
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(std::string_view(out.data(), result.ptr - out.data()), std::string_view("1a"));
	}
}

TEST(ToChars, LargeBase11)
{
	std::array<char, 145> out;
	MultiPrecision::Unsigned testee{(u({1}) << 500) + 1};
	std::to_chars_result result{to_chars(out.data(), out.data() + out.size(), testee, 11)};
	EXPECT_EQ(result.ec, std::errc{});
	EXPECT_EQ(
		std::string_view(out.data(), result.ptr - out.data()),
		std::string_view(
			"3648243702a02649400554188451578884198895404a17a733832544128a539248313272315363a824531310471884969a18176519a4588"
			"8a2604758012452530633727a33216a582"));
}

TEST(ToChars, Base36)
{
	{
		std::array<char, 1> out;
		MultiPrecision::Unsigned testee{u({})};
		std::to_chars_result result{to_chars(out.data(), out.data() + out.size(), testee, 36)};
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(std::string_view(out.data(), result.ptr - out.data()), std::string_view("0"));
	}
	{
		std::array<char, 1> out;
		MultiPrecision::Unsigned testee{u({35})};
		std::to_chars_result result{to_chars(out.data(), out.data() + out.size(), testee, 36)};
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(std::string_view(out.data(), result.ptr - out.data()), std::string_view("z"));
	}
	{
		std::array<char, 2> out;
		MultiPrecision::Unsigned testee{u({71})};
		std::to_chars_result result{to_chars(out.data(), out.data() + out.size(), testee, 36)};
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(std::string_view(out.data(), result.ptr - out.data()), std::string_view("1z"));
	}
}

TEST(ToChars, LargeBase36)
{
	std::array<char, 97> out;
	MultiPrecision::Unsigned testee{(u({1}) << 500) + 1};
	std::to_chars_result result{to_chars(out.data(), out.data() + out.size(), testee, 36)};
	EXPECT_EQ(result.ec, std::errc{});
	EXPECT_EQ(
		std::string_view(out.data(), result.ptr - out.data()),
		std::string_view("cvq393tim3rxslyji15jiga5c9n5dfrc0wqu97d4ehafezmryianfjqxnrlguouhx4xx673x7f0im8phvk992i4rk2nn5scn5"));
}

} // namespace MultiPrecision
