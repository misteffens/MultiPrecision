//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "utils.h"
#include <MultiPrecision/Unsigned.h>
#include <gtest/gtest.h>

namespace MultiPrecision {

TEST(EqualUnsigned, TRUE)
{
	EXPECT_TRUE(u({}) == u({}));
	EXPECT_TRUE(u({1, 2}) == u({1, 2}));
}

TEST(EqualUnsigned, FALSE)
{
	EXPECT_FALSE(u({1}) == u({}));
	EXPECT_FALSE(u({1}) == u({2}));
	EXPECT_FALSE(u({1, 2}) == u({1, 2, 3}));
}

} // namespace MultiPrecision
