//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "utils.h"
#include <MultiPrecision/Unsigned.h>
#include <gtest/gtest.h>

namespace MultiPrecision {

TEST(GreaterThanOrEqualUnsigned, TRUE)
{
	EXPECT_TRUE(u({1}) >= u({}));
	EXPECT_TRUE(u({1, 2, 3}) >= u({1, 2}));
	EXPECT_TRUE(u({1, 3}) >= u({1, 2}));
	EXPECT_TRUE(u({2, 2}) >= u({1, 2}));
	EXPECT_TRUE(u({}) >= u({}));
	EXPECT_TRUE(u({1, 2}) >= u({1, 2}));
}

TEST(GreaterThanOrEqualUnsigned, FALSE)
{
	EXPECT_FALSE(u({}) >= u({1}));
	EXPECT_FALSE(u({1, 2}) >= u({1, 2, 3}));
	EXPECT_FALSE(u({1, 2}) >= u({1, 3}));
	EXPECT_FALSE(u({1, 2}) >= u({2, 2}));
}

} // namespace MultiPrecision
