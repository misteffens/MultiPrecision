//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "MockMemoryResource.h"
#include "utils.h"
#include <MultiPrecision/Unsigned.h>
#include <gtest/gtest.h>

namespace MultiPrecision {

TEST(MinusWordType, Zero)
{
	EXPECT_EQ(u({}) - w{0}, u({}));
	EXPECT_EQ(u({1}) - w{0}, u({1}));
	EXPECT_EQ(u({1}) - w{1}, u({}));
	EXPECT_EQ(u({wMax, 1}) - w{0}, u({wMax, 1}));
}

TEST(MinusWordType, ZeroOverflow)
{
	EXPECT_THROW(u({}) - w{1}, std::range_error);
}

TEST(MinusWordType, NoBorrow)
{
	EXPECT_EQ(u({2}) - w{1}, u({1}));
	EXPECT_EQ(u({wMax}) - w{wMax - 1}, u({1}));
	EXPECT_EQ(u({wMax}) - w{1}, u({wMax - 1}));
	EXPECT_EQ(u({wMax, 1}) - w{1}, u({wMax - 1, 1}));
}

TEST(MinusWordType, Borrow)
{
	EXPECT_EQ(u({0, 1}) - wMax, u({1}));
	EXPECT_EQ(u({0, 1}) - w{1}, u({wMax}));
	EXPECT_EQ(u({wMax - 1, 1}) - wMax, u({wMax}));
	EXPECT_EQ(u({wMax - 1, 0, 1}) - wMax, u({wMax, wMax}));
}

TEST(MinusWordType, Overflow)
{
	EXPECT_THROW(u({1}) - wMax, std::range_error);
}

TEST(MinusWordType, Allocations)
{
	{
		testing::StrictMock<MockMemoryResource> r;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(0);
		u({}, r) - w{0};
	}
	{
		testing::StrictMock<MockMemoryResource> r;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(2);
		u({wMax, 1}, r) - w{0};
	}
	{
		testing::StrictMock<MockMemoryResource> r;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(2);
		u({wMax, 1}, r) - w{wMax - 1};
	}
}

TEST(MinusWordType, OutOfMemory)
{
	{
		testing::StrictMock<MockMemoryResource> r;
		testing::Sequence s;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(1).InSequence(s);
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
		EXPECT_THROW(u({wMax, 1}, r) - w{wMax - 1}, std::bad_alloc);
	}
}

} // namespace MultiPrecision
