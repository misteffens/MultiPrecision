//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "MockMemoryResource.h"
#include "utils.h"
#include <MultiPrecision/Unsigned.h>
#include <gtest/gtest.h>

namespace MultiPrecision {

TEST(ReducedModuloWordType, DividendZero)
{
	EXPECT_EQ(u({}) % w{1}, u({}));
	EXPECT_EQ(u({}) % wMax, u({}));
}

TEST(ReducedModuloWordType, DivisionByZero)
{
	EXPECT_THROW(u({}) % w{0}, std::range_error);
	EXPECT_THROW(u({1}) % w{0}, std::range_error);
	EXPECT_THROW(u({1, wMax - 1}) % w{0}, std::range_error);
}

TEST(ReducedModuloWordType, SmallWordDivisorNoRemainder)
{
	EXPECT_EQ(u({1}) % w{1}, u({}));
	EXPECT_EQ(u({1, 1}) % w{1}, u({}));
}

TEST(ReducedModuloWordType, LargeWordDivisorNoRemainder)
{
	EXPECT_EQ(u({1, wMax - 1}) % wMax, u({}));
	EXPECT_EQ(u({1, wMax, wMax - 1}) % wMax, u({}));
}

TEST(ReducedModuloWordType, SmallWordDivisorRemainder)
{
	EXPECT_EQ(u({1}) % w{2}, u({1}));
	EXPECT_EQ(u({1, 1}) % w{2}, u({1}));
}

TEST(ReducedModuloWordType, LargeWordDivisorRemainder)
{
	EXPECT_EQ(u({1, wMax - 1}) % w{wMax - 1}, u({1}));
	EXPECT_EQ(u({1, wMax, wMax - 1}) % w{wMax - 1}, u({3}));
}

TEST(ReducedModuloWordType, Allocations)
{
	{
		testing::StrictMock<MockMemoryResource> r;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(2);
		u({1, wMax, wMax - 1}, r) % w{wMax - 1};
	}
}

TEST(ReducedModuloWordType, OutOfMemory)
{
	{
		testing::StrictMock<MockMemoryResource> r;
		testing::Sequence s;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(1).InSequence(s);
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
		EXPECT_THROW(u({1, wMax, wMax - 1}, r) % w{wMax - 1}, std::bad_alloc);
	}
}

} // namespace MultiPrecision
