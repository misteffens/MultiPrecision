//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//
#ifndef Unsigned_utils_h_INCLUDED
#define Unsigned_utils_h_INCLUDED

#include <MultiPrecision/Unsigned.h>
#include <initializer_list>
#include <memory_resource>

namespace MultiPrecision {

using w = Unsigned::WordType;

constexpr std::size_t wDigits{std::numeric_limits<MultiPrecision::w>::digits};
constexpr w wMax{std::numeric_limits<w>::max()};

Unsigned u(std::initializer_list<w> words);
Unsigned u(std::initializer_list<w> words, std::pmr::memory_resource& memoryResource);

} // namespace MultiPrecision

#endif // Unsigned_utils_h_INCLUDED
