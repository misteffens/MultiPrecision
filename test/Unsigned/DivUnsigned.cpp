//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "MockMemoryResource.h"
#include "utils.h"
#include <MultiPrecision/Unsigned.h>
#include <gtest/gtest.h>

namespace MultiPrecision {

TEST(DivUnsigned, DividendZero)
{
	{
		Unsigned::DivisionByUnsignedResult result{u({}).div(u({1}))};
		EXPECT_EQ(result.quotient, u({}));
		EXPECT_EQ(result.remainder, u({}));
	}
	{
		Unsigned::DivisionByUnsignedResult result{u({}).div(u({wMax}))};
		EXPECT_EQ(result.quotient, u({}));
		EXPECT_EQ(result.remainder, u({}));
	}
	{
		Unsigned::DivisionByUnsignedResult result{u({}).div(u({1, wMax - 1}))};
		EXPECT_EQ(result.quotient, u({}));
		EXPECT_EQ(result.remainder, u({}));
	}
}

TEST(DivUnsigned, DivisionByZero)
{
	EXPECT_THROW(u({}).div(u({})), std::range_error);
	EXPECT_THROW(u({1}).div(u({})), std::range_error);
	EXPECT_THROW(u({1, wMax - 1}).div(u({})), std::range_error);
}

TEST(DivUnsigned, SmallWordDivisorNoRemainder)
{
	{
		Unsigned::DivisionByUnsignedResult result{u({1}).div(u({1}))};
		EXPECT_EQ(result.quotient, u({1}));
		EXPECT_EQ(result.remainder, u({}));
	}
	{
		Unsigned::DivisionByUnsignedResult result{u({1, 1}).div(u({1}))};
		EXPECT_EQ(result.quotient, u({1, 1}));
		EXPECT_EQ(result.remainder, u({}));
	}
	{
		Unsigned::DivisionByUnsignedResult result{u({1, 1}).div(u({1, 1}))};
		EXPECT_EQ(result.quotient, u({1}));
		EXPECT_EQ(result.remainder, u({}));
	}
	{
		Unsigned::DivisionByUnsignedResult result{u({1, 2, 2, 1}).div(u({1, 1}))};
		EXPECT_EQ(result.quotient, u({1, 1, 1}));
		EXPECT_EQ(result.remainder, u({}));
	}
	{
		Unsigned::DivisionByUnsignedResult result{u({1, 2, 2, 1}).div(u({1, 1, 1}))};
		EXPECT_EQ(result.quotient, u({1, 1}));
		EXPECT_EQ(result.remainder, u({}));
	}
	{
		Unsigned::DivisionByUnsignedResult result{u({1, 2, 3, 2, 1}).div(u({1, 1, 1}))};
		EXPECT_EQ(result.quotient, u({1, 1, 1}));
		EXPECT_EQ(result.remainder, u({}));
	}
}

TEST(DivUnsigned, LargeWordDivisorNoRemainder)
{
	{
		Unsigned::DivisionByUnsignedResult result{u({1, wMax - 1}).div(u({wMax}))};
		EXPECT_EQ(result.quotient, u({wMax}));
		EXPECT_EQ(result.remainder, u({}));
	}
	{
		Unsigned::DivisionByUnsignedResult result{u({1, wMax, wMax - 1}).div(u({wMax}))};
		EXPECT_EQ(result.quotient, u({wMax, wMax}));
		EXPECT_EQ(result.remainder, u({}));
	}
	{
		Unsigned::DivisionByUnsignedResult result{u({1, wMax, wMax - 1}).div(u({wMax, wMax}))};
		EXPECT_EQ(result.quotient, u({wMax}));
		EXPECT_EQ(result.remainder, u({}));
	}
	{
		Unsigned::DivisionByUnsignedResult result{u({1, 0, wMax, wMax - 1, wMax}).div(u({wMax, wMax}))};
		EXPECT_EQ(result.quotient, u({wMax, wMax, wMax}));
		EXPECT_EQ(result.remainder, u({}));
	}
	{
		Unsigned::DivisionByUnsignedResult result{u({1, 0, wMax, wMax - 1, wMax}).div(u({wMax, wMax, wMax}))};
		EXPECT_EQ(result.quotient, u({wMax, wMax}));
		EXPECT_EQ(result.remainder, u({}));
	}
	{
		Unsigned::DivisionByUnsignedResult result{u({1, 0, 0, wMax - 1, wMax, wMax}).div(u({wMax, wMax, wMax}))};
		EXPECT_EQ(result.quotient, u({wMax, wMax, wMax}));
		EXPECT_EQ(result.remainder, u({}));
	}
}

TEST(DivUnsigned, SmallWordDivisorRemainder)
{
	{
		Unsigned::DivisionByUnsignedResult result{u({1}).div(u({2}))};
		EXPECT_EQ(result.quotient, u({0}));
		EXPECT_EQ(result.remainder, u({1}));
	}
	{
		Unsigned::DivisionByUnsignedResult result{u({1, 1}).div(u({2}))};
		EXPECT_EQ(result.quotient, u({w{1} << (wDigits - 1)}));
		EXPECT_EQ(result.remainder, u({1}));
	}
	{
		Unsigned::DivisionByUnsignedResult result{u({1, 1}).div(u({0, 1}))};
		EXPECT_EQ(result.quotient, u({1}));
		EXPECT_EQ(result.remainder, u({1}));
	}
	{
		Unsigned::DivisionByUnsignedResult result{u({1, 2, 2, 1}).div(u({0, 1}))};
		EXPECT_EQ(result.quotient, u({2, 2, 1}));
		EXPECT_EQ(result.remainder, u({1}));
	}
	{
		Unsigned::DivisionByUnsignedResult result{u({1, 2, 2, 1}).div(u({1, 0, 1}))};
		EXPECT_EQ(result.quotient, u({2, 1}));
		EXPECT_EQ(result.remainder, u({wMax}));
	}
	{
		Unsigned::DivisionByUnsignedResult result{u({1, 2, 3, 2, 1}).div(u({1, 0, 1}))};
		EXPECT_EQ(result.quotient, u({1, 2, 1}));
		EXPECT_EQ(result.remainder, u({0, 0, 1}));
	}
}

TEST(DivUnsigned, LargeWordDivisorRemainder)
{
	{
		Unsigned::DivisionByUnsignedResult result{u({1, wMax - 1}).div(u({wMax - 1}))};
		EXPECT_EQ(result.quotient, u({0, 1}));
		EXPECT_EQ(result.remainder, u({1}));
	}
	{
		Unsigned::DivisionByUnsignedResult result{u({1, wMax, wMax - 1}).div(u({wMax - 1}))};
		EXPECT_EQ(result.quotient, u({1, 1, 1}));
		EXPECT_EQ(result.remainder, u({3}));
	}
	{
		Unsigned::DivisionByUnsignedResult result{u({1, wMax, wMax - 1}).div(u({wMax - 1, wMax}))};
		EXPECT_EQ(result.quotient, u({wMax}));
		EXPECT_EQ(result.remainder, u({wMax}));
	}
	{
		Unsigned::DivisionByUnsignedResult result{u({1, 0, wMax, wMax - 1, wMax}).div(u({wMax - 1, wMax}))};
		EXPECT_EQ(result.quotient, u({wMax, 0, 0, 1}));
		EXPECT_EQ(result.remainder, u({wMax, 1}));
	}
	{
		Unsigned::DivisionByUnsignedResult result{u({1, 0, wMax, wMax - 1, wMax}).div(u({wMax, wMax - 1, wMax}))};
		EXPECT_EQ(result.quotient, u({0, 0, 1}));
		EXPECT_EQ(result.remainder, u({1}));
	}
	{
		Unsigned::DivisionByUnsignedResult result{u({1, 0, 0, wMax - 1, wMax, wMax}).div(u({wMax, wMax - 1, wMax}))};
		EXPECT_EQ(result.quotient, u({wMax, 0, 0, 1}));
		EXPECT_EQ(result.remainder, u({0, 0, 1}));
	}
}

TEST(DivUnsigned, Allocations)
{
	{
		testing::StrictMock<MockMemoryResource> r1, r2;
		EXPECT_CALL(r1, do_allocate(testing::_, testing::_)).Times(4);
		EXPECT_CALL(r2, do_allocate(testing::_, testing::_)).Times(4);
		u({1, 0, 0, wMax - 1, wMax, wMax}, r1).div(u({wMax, wMax - 1, wMax}, r2));
	}
	{
		testing::StrictMock<MockMemoryResource> r1, r2;
		EXPECT_CALL(r1, do_allocate(testing::_, testing::_)).Times(3);
		EXPECT_CALL(r2, do_allocate(testing::_, testing::_)).Times(1);
		u({1, 0, 0, wMax - 1, wMax, wMax}, r1).div(u({wMax - 1}, r2));
	}
}

TEST(DivUnsigned, OutOfMemory)
{
	{
		testing::StrictMock<MockMemoryResource> r;
		testing::Sequence s;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(1).InSequence(s);
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
		EXPECT_THROW(u({1, 0, 0, wMax - 1, wMax, wMax}, r).div(u({wMax, wMax - 1, wMax})), std::bad_alloc);
	}
	{
		testing::StrictMock<MockMemoryResource> r;
		testing::Sequence s;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(2).InSequence(s);
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
		EXPECT_THROW(u({1, 0, 0, wMax - 1, wMax, wMax}, r).div(u({wMax, wMax - 1, wMax})), std::bad_alloc);
	}
	{
		testing::StrictMock<MockMemoryResource> r;
		testing::Sequence s;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(3).InSequence(s);
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
		EXPECT_THROW(u({1, 0, 0, wMax - 1, wMax, wMax}, r).div(u({wMax, wMax - 1, wMax})), std::bad_alloc);
	}
	{
		testing::StrictMock<MockMemoryResource> r;
		testing::Sequence s;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(1).InSequence(s);
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
		EXPECT_THROW(u({1, 0, 0, wMax - 1, wMax, wMax}).div(u({wMax, wMax - 1, wMax}, r)), std::bad_alloc);
	}
	{
		testing::StrictMock<MockMemoryResource> r;
		testing::Sequence s;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(2).InSequence(s);
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
		EXPECT_THROW(u({1, 0, 0, wMax - 1, wMax, wMax}).div(u({wMax, wMax - 1, wMax}, r)), std::bad_alloc);
	}
	{
		testing::StrictMock<MockMemoryResource> r;
		testing::Sequence s;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(3).InSequence(s);
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
		EXPECT_THROW(u({1, 0, 0, wMax - 1, wMax, wMax}).div(u({wMax, wMax - 1, wMax}, r)), std::bad_alloc);
	}
}

} // namespace MultiPrecision
