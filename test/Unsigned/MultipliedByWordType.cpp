//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "MockMemoryResource.h"
#include "utils.h"
#include <MultiPrecision/Unsigned.h>
#include <gtest/gtest.h>

namespace MultiPrecision {

TEST(MultipliedByWordType, Zero)
{
	EXPECT_EQ(u({}) * w{0}, u({}));
	EXPECT_EQ(u({1}) * w{0}, u({}));
	EXPECT_EQ(u({}) * w{1}, u({}));
	EXPECT_EQ(u({wMax, 1}) * w{0}, u({}));
}

TEST(MultipliedByWordType, SmallWords)
{
	EXPECT_EQ(u({1}) * w{1}, u({1}));
	EXPECT_EQ(u({1, 1}) * w{1}, u({1, 1}));
}

TEST(MultipliedByWordType, LargeWords)
{
	EXPECT_EQ(u({wMax}) * wMax, u({1, wMax - 1}));
	EXPECT_EQ(u({wMax, wMax}) * wMax, u({1, wMax, wMax - 1}));
}

TEST(MultipliedByWordType, Allocations)
{
	{
		testing::StrictMock<MockMemoryResource> r;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(0);
		u({}, r) * w{0};
	}
	{
		testing::StrictMock<MockMemoryResource> r;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(1);
		u({wMax, 1}, r) * w{0};
	}
	{
		testing::StrictMock<MockMemoryResource> r;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(2);
		u({wMax, wMax}, r) * w{wMax};
	}
}

TEST(MultipliedByWordType, OutOfMemory)
{
	{
		testing::StrictMock<MockMemoryResource> r;
		testing::Sequence s;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(1).InSequence(s);
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
		EXPECT_THROW(u({wMax, wMax}, r) * w{wMax}, std::bad_alloc);
	}
}

} // namespace MultiPrecision
