//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "MockMemoryResource.h"
#include "utils.h"
#include <MultiPrecision/Unsigned.h>
#include <gtest/gtest.h>

namespace MultiPrecision {

TEST(PlusWordType, Zero)
{
	EXPECT_EQ(u({}) + w{0}, u({}));
	EXPECT_EQ(u({1}) + w{0}, u({1}));
	EXPECT_EQ(u({}) + w{1}, u({1}));
	EXPECT_EQ(u({wMax, 1}) + w{0}, u({wMax, 1}));
}

TEST(PlusWordType, NoCarry)
{
	EXPECT_EQ(u({1}) + w{1}, u({2}));
	EXPECT_EQ(u({wMax - 1}) + w{1}, u({wMax}));
	EXPECT_EQ(u({1}) + w{wMax - 1}, u({wMax}));
	EXPECT_EQ(u({wMax - 1, 1}) + w{1}, u({wMax, 1}));
}

TEST(PlusWordType, Carry)
{
	EXPECT_EQ(u({wMax}) + w{1}, u({0, 1}));
	EXPECT_EQ(u({1}) + wMax, u({0, 1}));
	EXPECT_EQ(u({wMax}) + wMax, u({wMax - 1, 1}));
	EXPECT_EQ(u({wMax, wMax}) + wMax, u({wMax - 1, 0, 1}));
}

TEST(PlusWordType, Allocations)
{
	{
		testing::StrictMock<MockMemoryResource> r;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(1);
		u({}, r) + w{0};
	}
	{
		testing::StrictMock<MockMemoryResource> r;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(2);
		u({wMax, 1}, r) + w{0};
	}
	{
		testing::StrictMock<MockMemoryResource> r;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(2);
		u({wMax - 1, 1}, r) + w{1};
	}
}

TEST(PlusWordType, OutOfMemory)
{
	{
		testing::StrictMock<MockMemoryResource> r;
		testing::Sequence s;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(1).InSequence(s);
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
		EXPECT_THROW(u({wMax - 1, 1}, r) + w{1}, std::bad_alloc);
	}
}

} // namespace MultiPrecision
