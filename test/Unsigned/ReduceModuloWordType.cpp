//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "MockMemoryResource.h"
#include "utils.h"
#include <MultiPrecision/Unsigned.h>
#include <gtest/gtest.h>

namespace MultiPrecision {

TEST(ReduceModuloWordType, DividendZero)
{
	{
		Unsigned testee{u({})};
		EXPECT_EQ(&(testee %= w{1}), &testee);
		EXPECT_EQ(testee, u({}));
	}
	{
		Unsigned testee{u({})};
		EXPECT_EQ(&(testee %= wMax), &testee);
		EXPECT_EQ(testee, u({}));
	}
}

TEST(ReduceModuloWordType, DivisionByZero)
{
	{
		Unsigned testee{u({})};
		EXPECT_THROW(testee %= w{0}, std::range_error);
	}
	{
		Unsigned testee{u({1})};
		EXPECT_THROW(testee %= w{0}, std::range_error);
	}
	{
		Unsigned testee{u({1, wMax - 1})};
		EXPECT_THROW(testee %= w{0}, std::range_error);
	}
}

TEST(ReduceModuloWordType, SmallWordDivisorNoRemainder)
{
	{
		Unsigned testee{u({1})};
		EXPECT_EQ(&(testee %= w{1}), &testee);
		EXPECT_EQ(testee, u({}));
	}
	{
		Unsigned testee{u({1, 1})};
		EXPECT_EQ(&(testee %= w{1}), &testee);
		EXPECT_EQ(testee, u({}));
	}
}

TEST(ReduceModuloWordType, LargeWordDivisorNoRemainder)
{
	{
		Unsigned testee{u({1, wMax - 1})};
		EXPECT_EQ(&(testee %= wMax), &testee);
		EXPECT_EQ(testee, u({}));
	}
	{
		Unsigned testee{u({1, wMax, wMax - 1})};
		EXPECT_EQ(&(testee %= wMax), &testee);
		EXPECT_EQ(testee, u({}));
	}
}

TEST(ReduceModuloWordType, SmallWordDivisorRemainder)
{
	{
		Unsigned testee{u({1})};
		EXPECT_EQ(&(testee %= w{2}), &testee);
		EXPECT_EQ(testee, u({1}));
	}
	{
		Unsigned testee{u({1, 1})};
		EXPECT_EQ(&(testee %= w{2}), &testee);
		EXPECT_EQ(testee, u({1}));
	}
}

TEST(ReduceModuloWordType, LargeWordDivisorRemainder)
{
	{
		Unsigned testee{u({1, wMax - 1})};
		EXPECT_EQ(&(testee %= w{wMax - 1}), &testee);
		EXPECT_EQ(testee, u({1}));
	}
	{
		Unsigned testee{u({1, wMax, wMax - 1})};
		EXPECT_EQ(&(testee %= w{wMax - 1}), &testee);
		EXPECT_EQ(testee, u({3}));
	}
}

TEST(ReduceModuloWordType, Allocations)
{
	{
		testing::StrictMock<MockMemoryResource> r;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(2);
		Unsigned testee{u({1, wMax, wMax - 1}, r)};
		testee %= w{wMax - 1};
	}
}

TEST(ReduceModuloWordType, OutOfMemory)
{
	{
		testing::StrictMock<MockMemoryResource> r;
		testing::Sequence s;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(1).InSequence(s);
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
		Unsigned testee{u({1, wMax, wMax - 1}, r)};
		EXPECT_THROW(testee %= w{wMax - 1}, std::bad_alloc);
	}
}

} // namespace MultiPrecision
