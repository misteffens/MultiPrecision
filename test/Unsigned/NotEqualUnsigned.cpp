//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "utils.h"
#include <MultiPrecision/Unsigned.h>
#include <gtest/gtest.h>

namespace MultiPrecision {

TEST(NotEqualUnsigned, TRUE)
{
	EXPECT_TRUE(u({1}) != u({0}));
	EXPECT_TRUE(u({1}) != u({2}));
	EXPECT_TRUE(u({1, 2}) != u({1, 2, 3}));
}

TEST(NotEqualUnsigned, FALSE)
{
	EXPECT_FALSE(u({}) != u({}));
	EXPECT_FALSE(u({1, 2}) != u({1, 2}));
}

} // namespace MultiPrecision
