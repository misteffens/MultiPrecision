//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "utils.h"
#include <MultiPrecision/Unsigned.h>
#include <gtest/gtest.h>

namespace MultiPrecision {

TEST(ToHexString, Null)
{
	std::string sample{MultiPrecision::u({}).toHexString()};
	EXPECT_EQ(sample, std::string("0"));
}

TEST(ToHexString, MultiWord)
{
	std::string sample{MultiPrecision::u({1, 15}).toHexString()};
	EXPECT_EQ(sample.front(), 'F');
	EXPECT_EQ(sample.back(), '1');
	EXPECT_EQ(sample.size(), std::numeric_limits<MultiPrecision::w>::digits / 4 + 1);
}

} // namespace MultiPrecision
