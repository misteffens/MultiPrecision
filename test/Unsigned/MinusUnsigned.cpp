//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "MockMemoryResource.h"
#include "utils.h"
#include <MultiPrecision/Unsigned.h>
#include <gtest/gtest.h>

namespace MultiPrecision {

TEST(MinusUnsigned, Zero)
{
	EXPECT_EQ(u({}) - u({}), u({}));
	EXPECT_EQ(u({1}) - u({}), u({1}));
	EXPECT_EQ(u({1}) - u({1}), u({}));
	EXPECT_EQ(u({wMax, 1}) - u({}), u({wMax, 1}));
	EXPECT_EQ(u({wMax, 1}) - u({wMax, 1}), u({}));
}

TEST(MinusUnsigned, ZeroOverflow)
{
	EXPECT_THROW(u({}) - u({1}), std::range_error);
	EXPECT_THROW(u({}) - u({wMax, 1}), std::range_error);
}

TEST(MinusUnsigned, NoBorrow)
{
	EXPECT_EQ(u({2}) - u({1}), u({1}));
	EXPECT_EQ(u({wMax}) - u({wMax - 1}), u({1}));
	EXPECT_EQ(u({wMax}) - u({1}), u({wMax - 1}));
	EXPECT_EQ(u({wMax, 1}) - u({wMax - 1, 1}), u({1}));
	EXPECT_EQ(u({wMax, 1}) - u({1}), u({wMax - 1, 1}));
}

TEST(MinusUnsigned, Borrow)
{
	EXPECT_EQ(u({0, 1}) - u({wMax}), u({1}));
	EXPECT_EQ(u({0, 1}) - u({1}), u({wMax}));
	EXPECT_EQ(u({wMax - 1, 1}) - u({wMax}), u({wMax}));
	EXPECT_EQ(u({wMax - 1, 0, 1}) - u({wMax, wMax}), u({wMax}));
	EXPECT_EQ(u({wMax - 1, 0, 1}) - u({wMax}), u({wMax, wMax}));
}

TEST(MinusUnsigned, Overflow)
{
	EXPECT_THROW(u({1}) - u({wMax}), std::range_error);
	EXPECT_THROW(u({wMax}) - u({0, 1}), std::range_error);
	EXPECT_THROW(u({1}) - u({0, 1}), std::range_error);
	EXPECT_THROW(u({wMax}) - u({wMax - 1, 1}), std::range_error);
	EXPECT_THROW(u({wMax, wMax}) - u({wMax - 1, 0, 1}), std::range_error);
	EXPECT_THROW(u({wMax, wMax - 1}) - u({wMax - 1, wMax}), std::range_error);
}

TEST(MinusUnsigned, Allocations)
{
	{
		testing::StrictMock<MockMemoryResource> r1, r2;
		EXPECT_CALL(r1, do_allocate(testing::_, testing::_)).Times(0);
		EXPECT_CALL(r2, do_allocate(testing::_, testing::_)).Times(0);
		u({}, r1) - u({}, r2);
	}
	{
		testing::StrictMock<MockMemoryResource> r1, r2;
		EXPECT_CALL(r1, do_allocate(testing::_, testing::_)).Times(2);
		EXPECT_CALL(r2, do_allocate(testing::_, testing::_)).Times(0);
		u({wMax, 1}, r1) - u({}, r2);
	}
	{
		testing::StrictMock<MockMemoryResource> r1, r2;
		EXPECT_CALL(r1, do_allocate(testing::_, testing::_)).Times(2);
		EXPECT_CALL(r2, do_allocate(testing::_, testing::_)).Times(1);
		u({wMax, 1}, r1) - u({wMax - 1, 1}, r2);
	}
}

TEST(MinusUnsigned, OutOfMemory)
{
	{
		testing::StrictMock<MockMemoryResource> r;
		testing::Sequence s;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(1).InSequence(s);
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
		EXPECT_THROW(u({wMax, 1}, r) - u({wMax - 1, 1}), std::bad_alloc);
	}
}

} // namespace MultiPrecision
