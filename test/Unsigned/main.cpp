//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include <gtest/gtest.h>

int main(int argc, char* argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
