//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "MockMemoryResource.h"
#include "utils.h"
#include <MultiPrecision/Unsigned.h>
#include <gtest/gtest.h>

namespace MultiPrecision {

TEST(PlusUnsigned, Zero)
{
	EXPECT_EQ(u({}) + u({}), u({}));
	EXPECT_EQ(u({1}) + u({}), u({1}));
	EXPECT_EQ(u({}) + u({1}), u({1}));
	EXPECT_EQ(u({wMax, 1}) + u({}), u({wMax, 1}));
	EXPECT_EQ(u({}) + u({wMax, 1}), u({wMax, 1}));
}

TEST(PlusUnsigned, NoCarry)
{
	EXPECT_EQ(u({1}) + u({1}), u({2}));
	EXPECT_EQ(u({wMax - 1}) + u({1}), u({wMax}));
	EXPECT_EQ(u({1}) + u({wMax - 1}), u({wMax}));
	EXPECT_EQ(u({wMax - 1, 1}) + u({1}), u({wMax, 1}));
	EXPECT_EQ(u({1}) + u({wMax - 1, 1}), u({wMax, 1}));
}

TEST(PlusUnsigned, Carry)
{
	EXPECT_EQ(u({wMax}) + u({1}), u({0, 1}));
	EXPECT_EQ(u({1}) + u({wMax}), u({0, 1}));
	EXPECT_EQ(u({wMax}) + u({wMax}), u({wMax - 1, 1}));
	EXPECT_EQ(u({wMax, wMax}) + u({wMax}), u({wMax - 1, 0, 1}));
	EXPECT_EQ(u({wMax}) + u({wMax, wMax}), u({wMax - 1, 0, 1}));
}

TEST(PlusUnsigned, Allocations)
{
	{
		testing::StrictMock<MockMemoryResource> r1, r2;
		EXPECT_CALL(r1, do_allocate(testing::_, testing::_)).Times(1);
		EXPECT_CALL(r2, do_allocate(testing::_, testing::_)).Times(0);
		u({}, r1) + u({}, r2);
	}
	{
		testing::StrictMock<MockMemoryResource> r1, r2;
		EXPECT_CALL(r1, do_allocate(testing::_, testing::_)).Times(2);
		EXPECT_CALL(r2, do_allocate(testing::_, testing::_)).Times(0);
		u({wMax, 1}, r1) + u({}, r2);
	}
	{
		testing::StrictMock<MockMemoryResource> r1, r2;
		EXPECT_CALL(r1, do_allocate(testing::_, testing::_)).Times(2);
		EXPECT_CALL(r2, do_allocate(testing::_, testing::_)).Times(1);
		u({1}, r1) + u({wMax - 1, 1}, r2);
	}
}

TEST(PlusUnsigned, OutOfMemory)
{
	{
		testing::StrictMock<MockMemoryResource> r;
		testing::Sequence s;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(1).InSequence(s);
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
		EXPECT_THROW(u({1}, r) + u({wMax - 1, 1}), std::bad_alloc);
	}
}

} // namespace MultiPrecision
