//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "MockMemoryResource.h"
#include "utils.h"
#include <MultiPrecision/Unsigned.h>
#include <gtest/gtest.h>

namespace MultiPrecision {

TEST(IncrementByUnsigned, Zero)
{
	{
		Unsigned testee{u({})};
		EXPECT_EQ(&(testee += u({})), &testee);
		EXPECT_EQ(testee, u({}));
	}
	{
		Unsigned testee{u({1})};
		EXPECT_EQ(&(testee += u({})), &testee);
		EXPECT_EQ(testee, u({1}));
	}
	{
		Unsigned testee{u({})};
		EXPECT_EQ(&(testee += u({1})), &testee);
		EXPECT_EQ(testee, u({1}));
	}
	{
		Unsigned testee{u({wMax, 1})};
		EXPECT_EQ(&(testee += u({})), &testee);
		EXPECT_EQ(testee, u({wMax, 1}));
	}
	{
		Unsigned testee{u({})};
		EXPECT_EQ(&(testee += u({wMax, 1})), &testee);
		EXPECT_EQ(testee, u({wMax, 1}));
	}
}

TEST(IncrementByUnsigned, NoCarry)
{
	{
		Unsigned testee{u({1})};
		EXPECT_EQ(&(testee += u({1})), &testee);
		EXPECT_EQ(testee, u({2}));
	}
	{
		Unsigned testee{u({wMax - 1})};
		EXPECT_EQ(&(testee += u({1})), &testee);
		EXPECT_EQ(testee, u({wMax}));
	}
	{
		Unsigned testee{u({1})};
		EXPECT_EQ(&(testee += u({wMax - 1})), &testee);
		EXPECT_EQ(testee, u({wMax}));
	}
	{
		Unsigned testee{u({wMax - 1, 1})};
		EXPECT_EQ(&(testee += u({1})), &testee);
		EXPECT_EQ(testee, u({wMax, 1}));
	}
	{
		Unsigned testee{u({1})};
		EXPECT_EQ(&(testee += u({wMax - 1, 1})), &testee);
		EXPECT_EQ(testee, u({wMax, 1}));
	}
}

TEST(IncrementByUnsigned, Carry)
{
	{
		Unsigned testee{u({wMax})};
		EXPECT_EQ(&(testee += u({1})), &testee);
		EXPECT_EQ(testee, u({0, 1}));
	}
	{
		Unsigned testee{u({1})};
		EXPECT_EQ(&(testee += u({wMax})), &testee);
		EXPECT_EQ(testee, u({0, 1}));
	}
	{
		Unsigned testee{u({wMax})};
		EXPECT_EQ(&(testee += u({wMax})), &testee);
		EXPECT_EQ(testee, u({wMax - 1, 1}));
	}
	{
		Unsigned testee{u({wMax, wMax})};
		EXPECT_EQ(&(testee += u({wMax})), &testee);
		EXPECT_EQ(testee, u({wMax - 1, 0, 1}));
	}
	{
		Unsigned testee{u({wMax})};
		EXPECT_EQ(&(testee += u({wMax, wMax})), &testee);
		EXPECT_EQ(testee, u({wMax - 1, 0, 1}));
	}
}

TEST(IncrementByUnsigned, Allocations)
{
	{
		testing::StrictMock<MockMemoryResource> r1, r2;
		EXPECT_CALL(r1, do_allocate(testing::_, testing::_)).Times(1);
		EXPECT_CALL(r2, do_allocate(testing::_, testing::_)).Times(0);
		Unsigned testee{u({}, r1)};
		testee += u({}, r2);
	}
	{
		testing::StrictMock<MockMemoryResource> r1, r2;
		EXPECT_CALL(r1, do_allocate(testing::_, testing::_)).Times(2);
		EXPECT_CALL(r2, do_allocate(testing::_, testing::_)).Times(0);
		Unsigned testee{u({wMax, 1}, r1)};
		testee += u({}, r2);
	}
	{
		testing::StrictMock<MockMemoryResource> r1, r2;
		EXPECT_CALL(r1, do_allocate(testing::_, testing::_)).Times(2);
		EXPECT_CALL(r2, do_allocate(testing::_, testing::_)).Times(2);
		Unsigned testee{u({1}, r1)};
		testee += u({wMax - 1, 1}, r2);
		// No additional r1 allocation, as the space for the final carry was reserved with the previous increment.
		testee += u({wMax}, r2);
	}
}

TEST(IncrementByUnsigned, OutOfMemory)
{
	{
		testing::StrictMock<MockMemoryResource> r;
		testing::Sequence s;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(1).InSequence(s);
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
		Unsigned testee{u({1}, r)};
		EXPECT_THROW(testee += u({wMax - 1, 1}), std::bad_alloc);
	}
}

} // namespace MultiPrecision
