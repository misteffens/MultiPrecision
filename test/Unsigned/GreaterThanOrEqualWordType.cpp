//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "utils.h"
#include <MultiPrecision/Unsigned.h>
#include <gtest/gtest.h>

namespace MultiPrecision {

TEST(GreaterThanOrEqualWordType, TRUE)
{
	EXPECT_TRUE(u({}) >= 0);
	EXPECT_TRUE(u({1}) >= 0);
	EXPECT_TRUE(u({1}) >= 1);
	EXPECT_TRUE(u({2}) >= 1);
	EXPECT_TRUE(u({1, 2}) >= 2);
}

TEST(GreaterThanOrEqualWordType, FALSE)
{
	EXPECT_FALSE(u({}) >= 1);
	EXPECT_FALSE(u({1}) >= 2);
}

} // namespace MultiPrecision
