//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "MockMemoryResource.h"
#include "utils.h"
#include <MultiPrecision/Unsigned.h>
#include <gtest/gtest.h>

namespace MultiPrecision {

TEST(DivWordType, DividendZero)
{
	{
		Unsigned::DivisionByWordTypeResult result{u({}).div(w{1})};
		EXPECT_EQ(result.quotient, u({}));
		EXPECT_EQ(result.remainder, w{0});
	}
	{
		Unsigned::DivisionByWordTypeResult result{u({}).div(wMax)};
		EXPECT_EQ(result.quotient, u({}));
		EXPECT_EQ(result.remainder, w{0});
	}
}

TEST(DivWordType, DivisionByZero)
{
	EXPECT_THROW(u({}).div(w{0}), std::range_error);
	EXPECT_THROW(u({1}).div(w{0}), std::range_error);
	EXPECT_THROW(u({1, wMax - 1}).div(w{0}), std::range_error);
}

TEST(DivWordType, SmallWordDivisorNoRemainder)
{
	{
		Unsigned::DivisionByWordTypeResult result{u({1}).div(w{1})};
		EXPECT_EQ(result.quotient, u({1}));
		EXPECT_EQ(result.remainder, w{0});
	}
	{
		Unsigned::DivisionByWordTypeResult result{u({1, 1}).div(w{1})};
		EXPECT_EQ(result.quotient, u({1, 1}));
		EXPECT_EQ(result.remainder, w{0});
	}
}

TEST(DivWordType, LargeWordDivisorNoRemainder)
{
	{
		Unsigned::DivisionByWordTypeResult result{u({1, wMax - 1}).div(wMax)};
		EXPECT_EQ(result.quotient, u({wMax}));
		EXPECT_EQ(result.remainder, w{0});
	}
	{
		Unsigned::DivisionByWordTypeResult result{u({1, wMax, wMax - 1}).div(wMax)};
		EXPECT_EQ(result.quotient, u({wMax, wMax}));
		EXPECT_EQ(result.remainder, w{0});
	}
}

TEST(DivWordType, SmallWordDivisorRemainder)
{
	{
		Unsigned::DivisionByWordTypeResult result{u({1}).div(w{2})};
		EXPECT_EQ(result.quotient, u({0}));
		EXPECT_EQ(result.remainder, w{1});
	}
	{
		Unsigned::DivisionByWordTypeResult result{u({1, 1}).div(w{2})};
		EXPECT_EQ(result.quotient, u({w{1} << (wDigits - 1)}));
		EXPECT_EQ(result.remainder, w{1});
	}
}

TEST(DivWordType, LargeWordDivisorRemainder)
{
	{
		Unsigned::DivisionByWordTypeResult result{u({1, wMax - 1}).div(w{wMax - 1})};
		EXPECT_EQ(result.quotient, u({0, 1}));
		EXPECT_EQ(result.remainder, w{1});
	}
	{
		Unsigned::DivisionByWordTypeResult result{u({1, wMax, wMax - 1}).div(w{wMax - 1})};
		EXPECT_EQ(result.quotient, u({1, 1, 1}));
		EXPECT_EQ(result.remainder, w{3});
	}
}

TEST(DivWordType, Allocations)
{
	{
		testing::StrictMock<MockMemoryResource> r;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(2);
		u({1, wMax, wMax - 1}, r).div(w{wMax - 1});
	}
}

TEST(DivWordType, OutOfMemory)
{
	{
		testing::StrictMock<MockMemoryResource> r;
		testing::Sequence s;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(1).InSequence(s);
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
		EXPECT_THROW(u({1, wMax, wMax - 1}, r).div(w{wMax - 1}), std::bad_alloc);
	}
}

} // namespace MultiPrecision
