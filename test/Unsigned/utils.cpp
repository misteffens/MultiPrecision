//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "utils.h"
#include <MultiPrecision/Unsigned.h>

MultiPrecision::Unsigned MultiPrecision::u(std::initializer_list<w> words)
{
	return Unsigned(words.begin(), words.end(), std::pmr::polymorphic_allocator<Unsigned::WordType>{});
}

MultiPrecision::Unsigned MultiPrecision::u(std::initializer_list<w> words, std::pmr::memory_resource& memoryResource)
{
	return Unsigned(words.begin(), words.end(), std::pmr::polymorphic_allocator<Unsigned::WordType>{&memoryResource});
}
