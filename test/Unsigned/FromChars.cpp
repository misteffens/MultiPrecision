//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "utils.h"
#include <MultiPrecision/Unsigned.h>
#include <MultiPrecision/charconv.h>
#include <gtest/gtest.h>

namespace MultiPrecision {

TEST(FromChars, InvalidBase)
{
	{
		char const* in{"0"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee, 1)};
		EXPECT_EQ(result.ptr, in);
		EXPECT_EQ(result.ec, std::errc::invalid_argument);
		EXPECT_EQ(testee, u({1, 2}));
	}
	{
		char const* in{"0"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee, 37)};
		EXPECT_EQ(result.ptr, in);
		EXPECT_EQ(result.ec, std::errc::invalid_argument);
		EXPECT_EQ(testee, u({1, 2}));
	}
}

TEST(FromChars, Invalid)
{
	{
		char const* in{""};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee)};
		EXPECT_EQ(result.ptr, in);
		EXPECT_EQ(result.ec, std::errc::invalid_argument);
		EXPECT_EQ(testee, u({1, 2}));
	}
	{
		char const* in{" 234"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee)};
		EXPECT_EQ(result.ptr, in);
		EXPECT_EQ(result.ec, std::errc::invalid_argument);
		EXPECT_EQ(testee, u({1, 2}));
	}
	{
		char const* in{"a"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee)};
		EXPECT_EQ(result.ptr, in);
		EXPECT_EQ(result.ec, std::errc::invalid_argument);
		EXPECT_EQ(testee, u({1, 2}));
	}
	{
		char const* in{"A"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee)};
		EXPECT_EQ(result.ptr, in);
		EXPECT_EQ(result.ec, std::errc::invalid_argument);
		EXPECT_EQ(testee, u({1, 2}));
	}
	{
		char const* in{"9"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee, 7)};
		EXPECT_EQ(result.ptr, in);
		EXPECT_EQ(result.ec, std::errc::invalid_argument);
		EXPECT_EQ(testee, u({1, 2}));
	}
}

TEST(FromChars, Base2)
{
	{
		char const* in{"0"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee, 2)};
		EXPECT_EQ(result.ptr, in + 1);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({}));
	}
	{
		char const* in{"1"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee, 2)};
		EXPECT_EQ(result.ptr, in + 1);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({1}));
	}
	{
		char const* in{"1a"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee, 2)};
		EXPECT_EQ(result.ptr, in + 1);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({1}));
	}
	{
		char const* in{"1A"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee, 2)};
		EXPECT_EQ(result.ptr, in + 1);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({1}));
	}
	{
		char const* in{"1 "};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee, 2)};
		EXPECT_EQ(result.ptr, in + 1);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({1}));
	}
	{
		char const* in{"010011"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee, 2)};
		EXPECT_EQ(result.ptr, in + 6);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({19}));
	}
	{
		char const* in{"10011a"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee, 2)};
		EXPECT_EQ(result.ptr, in + 5);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({19}));
	}
	{
		char const* in{"10011A"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee, 2)};
		EXPECT_EQ(result.ptr, in + 5);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({19}));
	}
	{
		char const* in{"10011 "};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee, 2)};
		EXPECT_EQ(result.ptr, in + 5);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({19}));
	}
}

TEST(FromChars, LargeBase2)
{
	char const* in{"100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
				   "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
				   "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
				   "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
				   "000000000000000000000000000000000000000000000000000000001"};
	Unsigned testee{u({})};
	std::from_chars_result result{from_chars(in, in + strlen(in), testee, 2)};
	EXPECT_EQ(result.ec, std::errc{});
	EXPECT_EQ(testee, (u({1}) << 500) + 1);
}

TEST(FromChars, Base10)
{
	{
		char const* in{"0"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee)};
		EXPECT_EQ(result.ptr, in + 1);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({}));
	}
	{
		char const* in{"9"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee)};
		EXPECT_EQ(result.ptr, in + 1);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({9}));
	}
	{
		char const* in{"9a"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee)};
		EXPECT_EQ(result.ptr, in + 1);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({9}));
	}
	{
		char const* in{"9A"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee)};
		EXPECT_EQ(result.ptr, in + 1);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({9}));
	}
	{
		char const* in{"9 "};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee)};
		EXPECT_EQ(result.ptr, in + 1);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({9}));
	}
	{
		char const* in{"19"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee)};
		EXPECT_EQ(result.ptr, in + 2);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({19}));
	}
	{
		char const* in{"19a"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee)};
		EXPECT_EQ(result.ptr, in + 2);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({19}));
	}
	{
		char const* in{"19A"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee)};
		EXPECT_EQ(result.ptr, in + 2);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({19}));
	}
	{
		char const* in{"19 "};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee)};
		EXPECT_EQ(result.ptr, in + 2);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({19}));
	}
}

TEST(FromChars, LargeBase10)
{
	char const* in{"327339060789614187001318969682759915221664204604306478948329136809613379640467455488327009232590415715088668412"
				   "7560071009217256545885393053328527589377"};
	Unsigned testee{u({})};
	std::from_chars_result result{from_chars(in, in + strlen(in), testee)};
	EXPECT_EQ(result.ec, std::errc{});
	EXPECT_EQ(testee, (u({1}) << 500) + 1);
}

TEST(FromChars, Base11)
{
	{
		char const* in{"0"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee, 11)};
		EXPECT_EQ(result.ptr, in + 1);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({}));
	}
	{
		char const* in{"a"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee, 11)};
		EXPECT_EQ(result.ptr, in + 1);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({10}));
	}
	{
		char const* in{"ab"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee, 11)};
		EXPECT_EQ(result.ptr, in + 1);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({10}));
	}
	{
		char const* in{"AB"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee, 11)};
		EXPECT_EQ(result.ptr, in + 1);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({10}));
	}
	{
		char const* in{"a "};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee, 11)};
		EXPECT_EQ(result.ptr, in + 1);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({10}));
	}
	{
		char const* in{"01a"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee, 11)};
		EXPECT_EQ(result.ptr, in + 3);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({21}));
	}
	{
		char const* in{"1ab"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee, 11)};
		EXPECT_EQ(result.ptr, in + 2);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({21}));
	}
	{
		char const* in{"1AB"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee, 11)};
		EXPECT_EQ(result.ptr, in + 2);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({21}));
	}
	{
		char const* in{"1a "};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee, 11)};
		EXPECT_EQ(result.ptr, in + 2);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({21}));
	}
}

TEST(FromChars, LargeBase11)
{
	char const* in{"3648243702a02649400554188451578884198895404a17a733832544128a539248313272315363a824531310471884969a18176519a4588"
				   "8a2604758012452530633727a33216a582"};
	Unsigned testee{u({})};
	std::from_chars_result result{from_chars(in, in + strlen(in), testee, 11)};
	EXPECT_EQ(result.ec, std::errc{});
	EXPECT_EQ(testee, (u({1}) << 500) + 1);
}

TEST(FromChars, Base36)
{
	{
		char const* in{"0"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee, 36)};
		EXPECT_EQ(result.ptr, in + 1);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({}));
	}
	{
		char const* in{"z"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee, 36)};
		EXPECT_EQ(result.ptr, in + 1);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({35}));
	}
	{
		char const* in{"z_"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee, 36)};
		EXPECT_EQ(result.ptr, in + 1);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({35}));
	}
	{
		char const* in{"Z_"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee, 36)};
		EXPECT_EQ(result.ptr, in + 1);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({35}));
	}
	{
		char const* in{"z "};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee, 36)};
		EXPECT_EQ(result.ptr, in + 1);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({35}));
	}
	{
		char const* in{"01z"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee, 36)};
		EXPECT_EQ(result.ptr, in + 3);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({71}));
	}
	{
		char const* in{"1z_"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee, 36)};
		EXPECT_EQ(result.ptr, in + 2);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({71}));
	}
	{
		char const* in{"1Z_"};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee, 36)};
		EXPECT_EQ(result.ptr, in + 2);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({71}));
	}
	{
		char const* in{"1z "};
		Unsigned testee{u({1, 2})};
		std::from_chars_result result{from_chars(in, in + strlen(in), testee, 36)};
		EXPECT_EQ(result.ptr, in + 2);
		EXPECT_EQ(result.ec, std::errc{});
		EXPECT_EQ(testee, u({71}));
	}
}

TEST(FromChars, LargeBase36)
{
	char const* in{"cvq393tim3rxslyji15jiga5c9n5dfrc0wqu97d4ehafezmryianfjqxnrlguouhx4xx673x7f0im8phvk992i4rk2nn5scn5"};
	Unsigned testee{u({})};
	std::from_chars_result result{from_chars(in, in + strlen(in), testee, 36)};
	EXPECT_EQ(result.ec, std::errc{});
	EXPECT_EQ(testee, (u({1}) << 500) + 1);
}

} // namespace MultiPrecision
