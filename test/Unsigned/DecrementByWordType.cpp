//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "MockMemoryResource.h"
#include "utils.h"
#include <MultiPrecision/Unsigned.h>
#include <gtest/gtest.h>

namespace MultiPrecision {

TEST(DecrementByWordType, Zero)
{
	{
		Unsigned testee{u({})};
		EXPECT_EQ(&(testee -= w{0}), &testee);
		EXPECT_EQ(testee, u({}));
	}
	{
		Unsigned testee{u({1})};
		EXPECT_EQ(&(testee -= w{0}), &testee);
		EXPECT_EQ(testee, u({1}));
	}
	{
		Unsigned testee{u({1})};
		EXPECT_EQ(&(testee -= w{1}), &testee);
		EXPECT_EQ(testee, u({}));
	}
	{
		Unsigned testee{u({wMax, 1})};
		EXPECT_EQ(&(testee -= w{0}), &testee);
		EXPECT_EQ(testee, u({wMax, 1}));
	}
}

TEST(DecrementByWordType, ZeroOverflow)
{
	{
		Unsigned testee{u({})};
		EXPECT_THROW(testee -= w{1}, std::range_error);
	}
}

TEST(DecrementByWordType, NoBorrow)
{
	{
		Unsigned testee{u({2})};
		EXPECT_EQ(&(testee -= w{1}), &testee);
		EXPECT_EQ(testee, u({1}));
	}
	{
		Unsigned testee{u({wMax})};
		EXPECT_EQ(&(testee -= w{wMax - 1}), &testee);
		EXPECT_EQ(testee, u({1}));
	}
	{
		Unsigned testee{u({wMax})};
		EXPECT_EQ(&(testee -= w{1}), &testee);
		EXPECT_EQ(testee, u({wMax - 1}));
	}
	{
		Unsigned testee{u({wMax, 1})};
		EXPECT_EQ(&(testee -= w{1}), &testee);
		EXPECT_EQ(testee, u({wMax - 1, 1}));
	}
}

TEST(DecrementByWordType, Borrow)
{
	{
		Unsigned testee{u({0, 1})};
		EXPECT_EQ(&(testee -= wMax), &testee);
		EXPECT_EQ(testee, u({1}));
	}
	{
		Unsigned testee{u({0, 1})};
		EXPECT_EQ(&(testee -= w{1}), &testee);
		EXPECT_EQ(testee, u({wMax}));
	}
	{
		Unsigned testee{u({wMax - 1, 1})};
		EXPECT_EQ(&(testee -= wMax), &testee);
		EXPECT_EQ(testee, u({wMax}));
	}
	{
		Unsigned testee{u({wMax - 1, 0, 1})};
		EXPECT_EQ(&(testee -= wMax), &testee);
		EXPECT_EQ(testee, u({wMax, wMax}));
	}
}

TEST(DecrementByWordType, Overflow)
{
	{
		Unsigned testee{u({1})};
		EXPECT_THROW(testee -= wMax, std::range_error);
	}
}

TEST(DecrementByWordType, Allocations)
{
	{
		testing::StrictMock<MockMemoryResource> r;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(0);
		Unsigned testee{u({}, r)};
		testee -= w{0};
	}
	{
		testing::StrictMock<MockMemoryResource> r;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(1);
		Unsigned testee{u({wMax, 1}, r)};
		testee -= w{0};
	}
	{
		testing::StrictMock<MockMemoryResource> r;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(1);
		Unsigned testee{u({wMax, 1}, r)};
		testee -= w{wMax - 1};
	}
}

} // namespace MultiPrecision
