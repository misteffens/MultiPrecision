//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "MockMemoryResource.h"
#include "utils.h"
#include <MultiPrecision/Unsigned.h>
#include <gtest/gtest.h>

namespace MultiPrecision {

TEST(MultiplyByWordType, Zero)
{
	{
		Unsigned testee{u({})};
		EXPECT_EQ(&(testee *= w{0}), &testee);
		EXPECT_EQ(testee, u({}));
	}
	{
		Unsigned testee{u({1})};
		EXPECT_EQ(&(testee *= w{0}), &testee);
		EXPECT_EQ(testee, u({}));
	}
	{
		Unsigned testee{u({})};
		EXPECT_EQ(&(testee *= w{1}), &testee);
		EXPECT_EQ(testee, u({}));
	}
	{
		Unsigned testee{u({wMax, 1})};
		EXPECT_EQ(&(testee *= w{0}), &testee);
		EXPECT_EQ(testee, u({}));
	}
}

TEST(MultiplyByWordType, SmallWords)
{
	{
		Unsigned testee{u({1})};
		EXPECT_EQ(&(testee *= w{1}), &testee);
		EXPECT_EQ(testee, u({1}));
	}
	{
		Unsigned testee{u({1, 1})};
		EXPECT_EQ(&(testee *= w{1}), &testee);
		EXPECT_EQ(testee, u({1, 1}));
	}
}

TEST(MultiplyByWordType, LargeWords)
{
	{
		Unsigned testee{u({wMax})};
		EXPECT_EQ(&(testee *= wMax), &testee);
		EXPECT_EQ(testee, u({1, wMax - 1}));
	}
	{
		Unsigned testee{u({wMax, wMax})};
		EXPECT_EQ(&(testee *= wMax), &testee);
		EXPECT_EQ(testee, u({1, wMax, wMax - 1}));
	}
}

TEST(MultiplyByWordType, Allocations)
{
	{
		testing::StrictMock<MockMemoryResource> r;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(0);
		Unsigned testee{u({}, r)};
		testee *= w{0};
	}
	{
		testing::StrictMock<MockMemoryResource> r;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(1);
		Unsigned testee{u({wMax, 1}, r)};
		testee *= w{0};
	}
	{
		testing::StrictMock<MockMemoryResource> r;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(2);
		Unsigned testee{u({wMax, wMax}, r)};
		testee *= w{wMax};
	}
}

TEST(MultiplyByWordType, OutOfMemory)
{
	{
		testing::StrictMock<MockMemoryResource> r;
		testing::Sequence s;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(1).InSequence(s);
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
		Unsigned testee{u({wMax, wMax}, r)};
		EXPECT_THROW(testee *= w{wMax}, std::bad_alloc);
	}
}

} // namespace MultiPrecision
