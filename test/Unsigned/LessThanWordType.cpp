//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "utils.h"
#include <MultiPrecision/Unsigned.h>
#include <gtest/gtest.h>

namespace MultiPrecision {

TEST(LessThanWordType, TRUE)
{
	EXPECT_TRUE(u({}) < 1);
	EXPECT_TRUE(u({1}) < 2);
}

TEST(LessThanWordType, FALSE)
{
	EXPECT_FALSE(u({}) < 0);
	EXPECT_FALSE(u({1}) < 0);
	EXPECT_FALSE(u({1}) < 1);
	EXPECT_FALSE(u({1, 2}) < 2);
}

} // namespace MultiPrecision
