//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "MockMemoryResource.h"
#include "Unsigned/utils.h"
#include <MultiPrecision/Unsigned.h>
#include <gtest/gtest.h>

namespace MultiPrecision {

TEST(DecrementByUnsigned, Zero)
{
	{
		Unsigned testee{u({})};
		EXPECT_EQ(&(testee -= u({})), &testee);
		EXPECT_EQ(testee, u({}));
	}
	{
		Unsigned testee{u({1})};
		EXPECT_EQ(&(testee -= u({})), &testee);
		EXPECT_EQ(testee, u({1}));
	}
	{
		Unsigned testee{u({1})};
		EXPECT_EQ(&(testee -= u({1})), &testee);
		EXPECT_EQ(testee, u({}));
	}
	{
		Unsigned testee{u({wMax, 1})};
		EXPECT_EQ(&(testee -= u({})), &testee);
		EXPECT_EQ(testee, u({wMax, 1}));
	}
	{
		Unsigned testee{u({wMax, 1})};
		EXPECT_EQ(&(testee -= u({wMax, 1})), &testee);
		EXPECT_EQ(testee, u({}));
	}
}

TEST(DecrementByUnsigned, ZeroOverflow)
{
	{
		Unsigned testee{u({})};
		EXPECT_THROW(testee -= u({1}), std::range_error);
	}
	{
		Unsigned testee{u({})};
		EXPECT_THROW(testee -= u({wMax, 1}), std::range_error);
	}
}

TEST(DecrementByUnsigned, NoBorrow)
{
	{
		Unsigned testee{u({2})};
		EXPECT_EQ(&(testee -= u({1})), &testee);
		EXPECT_EQ(testee, u({1}));
	}
	{
		Unsigned testee{u({wMax})};
		EXPECT_EQ(&(testee -= u({wMax - 1})), &testee);
		EXPECT_EQ(testee, u({1}));
	}
	{
		Unsigned testee{u({wMax})};
		EXPECT_EQ(&(testee -= u({1})), &testee);
		EXPECT_EQ(testee, u({wMax - 1}));
	}
	{
		Unsigned testee{u({wMax, 1})};
		EXPECT_EQ(&(testee -= u({wMax - 1, 1})), &testee);
		EXPECT_EQ(testee, u({1}));
	}
	{
		Unsigned testee{u({wMax, 1})};
		EXPECT_EQ(&(testee -= u({1})), &testee);
		EXPECT_EQ(testee, u({wMax - 1, 1}));
	}
}

TEST(DecrementByUnsigned, Borrow)
{
	{
		Unsigned testee{u({0, 1})};
		EXPECT_EQ(&(testee -= u({wMax})), &testee);
		EXPECT_EQ(testee, u({1}));
	}
	{
		Unsigned testee{u({0, 1})};
		EXPECT_EQ(&(testee -= u({1})), &testee);
		EXPECT_EQ(testee, u({wMax}));
	}
	{
		Unsigned testee{u({wMax - 1, 1})};
		EXPECT_EQ(&(testee -= u({wMax})), &testee);
		EXPECT_EQ(testee, u({wMax}));
	}
	{
		Unsigned testee{u({wMax - 1, 0, 1})};
		EXPECT_EQ(&(testee -= u({wMax, wMax})), &testee);
		EXPECT_EQ(testee, u({wMax}));
	}
	{
		Unsigned testee{u({wMax - 1, 0, 1})};
		EXPECT_EQ(&(testee -= u({wMax})), &testee);
		EXPECT_EQ(testee, u({wMax, wMax}));
	}
}

TEST(DecrementByUnsigned, Overflow)
{
	{
		Unsigned testee{u({1})};
		EXPECT_THROW(testee -= u({wMax}), std::range_error);
	}
	{
		Unsigned testee{u({wMax})};
		EXPECT_THROW(testee -= u({0, 1}), std::range_error);
	}
	{
		Unsigned testee{u({1})};
		EXPECT_THROW(testee -= u({0, 1}), std::range_error);
	}
	{
		Unsigned testee{u({wMax})};
		EXPECT_THROW(testee -= u({wMax - 1, 1}), std::range_error);
	}
	{
		Unsigned testee{u({wMax, wMax})};
		EXPECT_THROW(testee -= u({wMax - 1, 0, 1}), std::range_error);
	}
	{
		Unsigned testee{u({wMax, wMax - 1})};
		EXPECT_THROW(testee -= u({wMax - 1, wMax}), std::range_error);
	}
}

TEST(DecrementByUnsigned, Allocations)
{
	{
		testing::StrictMock<MockMemoryResource> r1, r2;
		EXPECT_CALL(r1, do_allocate(testing::_, testing::_)).Times(0);
		EXPECT_CALL(r2, do_allocate(testing::_, testing::_)).Times(0);
		Unsigned testee{u({}, r1)};
		testee -= u({}, r2);
	}
	{
		testing::StrictMock<MockMemoryResource> r1, r2;
		EXPECT_CALL(r1, do_allocate(testing::_, testing::_)).Times(1);
		EXPECT_CALL(r2, do_allocate(testing::_, testing::_)).Times(0);
		Unsigned testee{u({wMax, 1}, r1)};
		testee -= u({}, r2);
	}
	{
		testing::StrictMock<MockMemoryResource> r1, r2;
		EXPECT_CALL(r1, do_allocate(testing::_, testing::_)).Times(1);
		EXPECT_CALL(r2, do_allocate(testing::_, testing::_)).Times(1);
		Unsigned testee{u({wMax, 1}, r1)};
		testee -= u({wMax - 1, 1}, r2);
	}
}

} // namespace MultiPrecision
