//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "MockMemoryResource.h"
#include "utils.h"
#include <MultiPrecision/Unsigned.h>
#include <gtest/gtest.h>

namespace MultiPrecision {

TEST(MultipliedByUnsigned, Zero)
{
	EXPECT_EQ(u({}) * u({}), u({}));
	EXPECT_EQ(u({1}) * u({}), u({}));
	EXPECT_EQ(u({}) * u({1}), u({}));
	EXPECT_EQ(u({wMax, 1}) * u({}), u({}));
	EXPECT_EQ(u({}) * u({wMax, 1}), u({}));
}

TEST(MultipliedByUnsigned, SmallWords)
{
	EXPECT_EQ(u({1}) * u({1}), u({1}));
	EXPECT_EQ(u({1}) * u({1, 1}), u({1, 1}));
	EXPECT_EQ(u({1, 1}) * u({1}), u({1, 1}));
	EXPECT_EQ(u({1, 1}) * u({1, 1, 1}), u({1, 2, 2, 1}));
	EXPECT_EQ(u({1, 1, 1}) * u({1, 1}), u({1, 2, 2, 1}));
	EXPECT_EQ(u({1, 1, 1}) * u({1, 1, 1}), u({1, 2, 3, 2, 1}));
}

TEST(MultipliedByUnsigned, LargeWords)
{
	EXPECT_EQ(u({wMax}) * u({wMax}), u({1, wMax - 1}));
	EXPECT_EQ(u({wMax, wMax}) * u({wMax}), u({1, wMax, wMax - 1}));
	EXPECT_EQ(u({wMax}) * u({wMax, wMax}), u({1, wMax, wMax - 1}));
	EXPECT_EQ(u({wMax, wMax, wMax}) * u({wMax, wMax}), u({1, 0, wMax, wMax - 1, wMax}));
	EXPECT_EQ(u({wMax, wMax}) * u({wMax, wMax, wMax}), u({1, 0, wMax, wMax - 1, wMax}));
	EXPECT_EQ(u({wMax, wMax, wMax}) * u({wMax, wMax, wMax}), u({1, 0, 0, wMax - 1, wMax, wMax}));
}

TEST(MultipliedByUnsigned, Allocations)
{
	{
		testing::StrictMock<MockMemoryResource> r1, r2;
		EXPECT_CALL(r1, do_allocate(testing::_, testing::_)).Times(0);
		EXPECT_CALL(r2, do_allocate(testing::_, testing::_)).Times(0);
		u({}, r1) * u({}, r2);
	}
	{
		testing::StrictMock<MockMemoryResource> r1, r2;
		EXPECT_CALL(r1, do_allocate(testing::_, testing::_)).Times(2);
		EXPECT_CALL(r2, do_allocate(testing::_, testing::_)).Times(0);
		u({wMax, 1}, r1) * u({}, r2);
	}
	{
		testing::StrictMock<MockMemoryResource> r1, r2;
		EXPECT_CALL(r1, do_allocate(testing::_, testing::_)).Times(2);
		EXPECT_CALL(r2, do_allocate(testing::_, testing::_)).Times(1);
		u({wMax, wMax}, r1) * u({wMax, wMax, wMax}, r2);
	}
}

TEST(MultipliedByUnsigned, OutOfMemory)
{
	{
		testing::StrictMock<MockMemoryResource> r;
		testing::Sequence s;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(1).InSequence(s);
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
		EXPECT_THROW(u({wMax, wMax}, r) * u({wMax, wMax, wMax}), std::bad_alloc);
	}
}

} // namespace MultiPrecision
