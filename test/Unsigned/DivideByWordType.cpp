//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "MockMemoryResource.h"
#include "utils.h"
#include <MultiPrecision/Unsigned.h>
#include <gtest/gtest.h>

namespace MultiPrecision {

TEST(DivideByWordType, DividendZero)
{
	{
		Unsigned testee{u({})};
		EXPECT_EQ(&(testee /= w{1}), &testee);
		EXPECT_EQ(testee, u({}));
	}
	{
		Unsigned testee{u({})};
		EXPECT_EQ(&(testee /= wMax), &testee);
		EXPECT_EQ(testee, u({}));
	}
}

TEST(DivideByWordType, DivisionByZero)
{
	{
		Unsigned testee{u({})};
		EXPECT_THROW(testee /= w{0}, std::range_error);
	}
	{
		Unsigned testee{u({1})};
		EXPECT_THROW(testee /= w{0}, std::range_error);
	}
	{
		Unsigned testee{u({1, wMax - 1})};
		EXPECT_THROW(testee /= w{0}, std::range_error);
	}
}

TEST(DivideByWordType, SmallWordDivisorNoRemainder)
{
	{
		Unsigned testee{u({1})};
		EXPECT_EQ(&(testee /= w{1}), &testee);
		EXPECT_EQ(testee, u({1}));
	}
	{
		Unsigned testee{u({1, 1})};
		EXPECT_EQ(&(testee /= w{1}), &testee);
		EXPECT_EQ(testee, u({1, 1}));
	}
}

TEST(DivideByWordType, LargeWordDivisorNoRemainder)
{
	{
		Unsigned testee{u({1, wMax - 1})};
		EXPECT_EQ(&(testee /= wMax), &testee);
		EXPECT_EQ(testee, u({wMax}));
	}
	{
		Unsigned testee{u({1, wMax, wMax - 1})};
		EXPECT_EQ(&(testee /= wMax), &testee);
		EXPECT_EQ(testee, u({wMax, wMax}));
	}
}

TEST(DivideByWordType, SmallWordDivisorRemainder)
{
	{
		Unsigned testee{u({1})};
		EXPECT_EQ(&(testee /= w{2}), &testee);
		EXPECT_EQ(testee, u({0}));
	}
	{
		Unsigned testee{u({1, 1})};
		EXPECT_EQ(&(testee /= w{2}), &testee);
		EXPECT_EQ(testee, u({w{1} << (wDigits - 1)}));
	}
}

TEST(DivideByWordType, LargeWordDivisorRemainder)
{
	{
		Unsigned testee{u({1, wMax - 1})};
		EXPECT_EQ(&(testee /= w{wMax - 1}), &testee);
		EXPECT_EQ(testee, u({0, 1}));
	}
	{
		Unsigned testee{u({1, wMax, wMax - 1})};
		EXPECT_EQ(&(testee /= w{wMax - 1}), &testee);
		EXPECT_EQ(testee, u({1, 1, 1}));
	}
}

TEST(DivideByWordType, Allocations)
{
	{
		testing::StrictMock<MockMemoryResource> r;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(2);
		Unsigned testee{u({1, wMax, wMax - 1}, r)};
		testee /= w{wMax - 1};
	}
}

TEST(DivideByWordType, OutOfMemory)
{
	{
		testing::StrictMock<MockMemoryResource> r;
		testing::Sequence s;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(1).InSequence(s);
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
		Unsigned testee{u({1, wMax, wMax - 1}, r)};
		EXPECT_THROW(testee /= w{wMax - 1}, std::bad_alloc);
	}
}

} // namespace MultiPrecision
