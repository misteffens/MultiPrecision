//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "utils.h"
#include <MultiPrecision/Unsigned.h>
#include <gtest/gtest.h>

namespace MultiPrecision {

TEST(ShiftedRight, ByZero)
{
	EXPECT_EQ(u({1}) >> 0, u({1}));
}

TEST(ShiftedRight, ByDigits)
{
	EXPECT_EQ(u({1}) >> 1, u({0}));
	EXPECT_EQ(u({2}) >> 1, u({1}));
	EXPECT_EQ(u({w{1} << (wDigits - 1)}) >> (wDigits - 1), u({1}));
}

TEST(ShiftedRight, ByMultipleOfWords)
{
	EXPECT_EQ(u({1}) >> wDigits, u({}));
	EXPECT_EQ(u({0, 1}) >> wDigits, u({1}));
	EXPECT_EQ(u({0, 1}) >> (2 * wDigits), u({}));
	EXPECT_EQ(u({0, 1, 1}) >> wDigits, u({1, 1}));
	EXPECT_EQ(u({0, 0, 1}) >> (2 * wDigits), u({1}));
	EXPECT_EQ(u({0, 0, 1, 1}) >> (2 * wDigits), u({1, 1}));
}

TEST(ShiftedRight, ByMultipleOfWordsAndDigits)
{
	EXPECT_EQ(u({wMax - 1, 1}) >> (wDigits + 1), u({}));
	EXPECT_EQ(u({wMax - 1, 1}) >> 1, u({wMax}));
	EXPECT_EQ(u({0, wMax - 1, 1}) >> (wDigits + 1), u({wMax}));
	EXPECT_EQ(u({0, w{1} << (wDigits - 1)}) >> (2 * wDigits - 1), u({1}));
}

} // namespace MultiPrecision
