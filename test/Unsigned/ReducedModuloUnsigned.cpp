//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "MockMemoryResource.h"
#include "utils.h"
#include <MultiPrecision/Unsigned.h>
#include <gtest/gtest.h>

namespace MultiPrecision {

TEST(ReducedModuloUnsigned, DividendZero)
{
	EXPECT_EQ(u({}) % u({1}), u({}));
	EXPECT_EQ(u({}) % u({wMax}), u({}));
	EXPECT_EQ(u({}) % u({1, wMax - 1}), u({}));
}

TEST(ReducedModuloUnsigned, DivisionByZero)
{
	EXPECT_THROW(u({}) % u({}), std::range_error);
	EXPECT_THROW(u({1}) % u({}), std::range_error);
	EXPECT_THROW(u({1, wMax - 1}) % u({}), std::range_error);
}

TEST(ReducedModuloUnsigned, SmallWordDivisorNoRemainder)
{
	EXPECT_EQ(u({1}) % u({1}), u({}));
	EXPECT_EQ(u({1, 1}) % u({1}), u({}));
	EXPECT_EQ(u({1, 1}) % u({1, 1}), u({}));
	EXPECT_EQ(u({1, 2, 2, 1}) % u({1, 1}), u({}));
	EXPECT_EQ(u({1, 2, 2, 1}) % u({1, 1, 1}), u({}));
	EXPECT_EQ(u({1, 2, 3, 2, 1}) % u({1, 1, 1}), u({}));
}

TEST(ReducedModuloUnsigned, LargeWordDivisorNoRemainder)
{
	EXPECT_EQ(u({1, wMax - 1}) % u({wMax}), u({}));
	EXPECT_EQ(u({1, wMax, wMax - 1}) % u({wMax}), u({}));
	EXPECT_EQ(u({1, wMax, wMax - 1}) % u({wMax, wMax}), u({}));
	EXPECT_EQ(u({1, 0, wMax, wMax - 1, wMax}) % u({wMax, wMax}), u({}));
	EXPECT_EQ(u({1, 0, wMax, wMax - 1, wMax}) % u({wMax, wMax, wMax}), u({}));
	EXPECT_EQ(u({1, 0, 0, wMax - 1, wMax, wMax}) % u({wMax, wMax, wMax}), u({}));
}

TEST(ReducedModuloUnsigned, SmallWordDivisorRemainder)
{
	EXPECT_EQ(u({1}) % u({2}), u({1}));
	EXPECT_EQ(u({1, 1}) % u({2}), u({1}));
	EXPECT_EQ(u({1, 1}) % u({0, 1}), u({1}));
	EXPECT_EQ(u({1, 2, 2, 1}) % u({0, 1}), u({1}));
	EXPECT_EQ(u({1, 2, 2, 1}) % u({1, 0, 1}), u({wMax}));
	EXPECT_EQ(u({1, 2, 3, 2, 1}) % u({1, 0, 1}), u({0, 0, 1}));
}

TEST(ReducedModuloUnsigned, LargeWordDivisorRemainder)
{
	EXPECT_EQ(u({1, wMax - 1}) % u({wMax - 1}), u({1}));
	EXPECT_EQ(u({1, wMax, wMax - 1}) % u({wMax - 1}), u({3}));
	EXPECT_EQ(u({1, wMax, wMax - 1}) % u({wMax - 1, wMax}), u({wMax}));
	EXPECT_EQ(u({1, 0, wMax, wMax - 1, wMax}) % u({wMax - 1, wMax}), u({wMax, 1}));
	EXPECT_EQ(u({1, 0, wMax, wMax - 1, wMax}) % u({wMax, wMax - 1, wMax}), u({1}));
	EXPECT_EQ(u({1, 0, 0, wMax - 1, wMax, wMax}) % u({wMax, wMax - 1, wMax}), u({0, 0, 1}));
}

TEST(ReducedModuloUnsigned, Allocations)
{
	{
		testing::StrictMock<MockMemoryResource> r1, r2;
		EXPECT_CALL(r1, do_allocate(testing::_, testing::_)).Times(3);
		EXPECT_CALL(r2, do_allocate(testing::_, testing::_)).Times(4);
		u({1, 0, 0, wMax - 1, wMax, wMax}, r1) % u({wMax, wMax - 1, wMax}, r2);
	}
	{
		testing::StrictMock<MockMemoryResource> r1, r2;
		EXPECT_CALL(r1, do_allocate(testing::_, testing::_)).Times(2);
		EXPECT_CALL(r2, do_allocate(testing::_, testing::_)).Times(1);
		u({1, 0, 0, wMax - 1, wMax, wMax}, r1) % u({wMax - 1}, r2);
	}
}

TEST(ReducedModuloUnsigned, OutOfMemory)
{
	{
		testing::StrictMock<MockMemoryResource> r;
		testing::Sequence s;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(1).InSequence(s);
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
		EXPECT_THROW(u({1, 0, 0, wMax - 1, wMax, wMax}, r) % u({wMax, wMax - 1, wMax}), std::bad_alloc);
	}
	{
		testing::StrictMock<MockMemoryResource> r;
		testing::Sequence s;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(2).InSequence(s);
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
		EXPECT_THROW(u({1, 0, 0, wMax - 1, wMax, wMax}, r) % u({wMax, wMax - 1, wMax}), std::bad_alloc);
	}
	{
		testing::StrictMock<MockMemoryResource> r;
		testing::Sequence s;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(1).InSequence(s);
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
		EXPECT_THROW(u({1, 0, 0, wMax - 1, wMax, wMax}) % u({wMax, wMax - 1, wMax}, r), std::bad_alloc);
	}
	{
		testing::StrictMock<MockMemoryResource> r;
		testing::Sequence s;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(2).InSequence(s);
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
		EXPECT_THROW(u({1, 0, 0, wMax - 1, wMax, wMax}) % u({wMax, wMax - 1, wMax}, r), std::bad_alloc);
	}
	{
		testing::StrictMock<MockMemoryResource> r;
		testing::Sequence s;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(3).InSequence(s);
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
		EXPECT_THROW(u({1, 0, 0, wMax - 1, wMax, wMax}) % u({wMax, wMax - 1, wMax}, r), std::bad_alloc);
	}
}

} // namespace MultiPrecision
