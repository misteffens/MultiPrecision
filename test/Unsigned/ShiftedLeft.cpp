//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "utils.h"
#include <MultiPrecision/Unsigned.h>
#include <gtest/gtest.h>

namespace MultiPrecision {

TEST(ShiftedLeft, ByZero)
{
	EXPECT_EQ(u({1}) << 0, u({1}));
}

TEST(ShiftedLeft, ByDigits)
{
	EXPECT_EQ(u({1}) << 1, u({2}));
	EXPECT_EQ(u({1}) << (wDigits - 1), u({w{1} << (wDigits - 1)}));
}

TEST(ShiftedLeft, ByMultipleOfWords)
{
	EXPECT_EQ(u({1}) << wDigits, u({0, 1}));
	EXPECT_EQ(u({1, 1}) << wDigits, u({0, 1, 1}));
	EXPECT_EQ(u({1}) << (2 * wDigits), u({0, 0, 1}));
	EXPECT_EQ(u({1, 1}) << (2 * wDigits), u({0, 0, 1, 1}));
}

TEST(ShiftedLeft, ByMultipleOfWordsAndDigits)
{
	EXPECT_EQ(u({wMax}) << 1, u({wMax - 1, 1}));
	EXPECT_EQ(u({wMax}) << (wDigits + 1), u({0, wMax - 1, 1}));
	EXPECT_EQ(u({1}) << (2 * wDigits - 1), u({0, w{1} << (wDigits - 1)}));
}

} // namespace MultiPrecision
