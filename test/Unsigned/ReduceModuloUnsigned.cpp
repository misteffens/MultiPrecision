//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "MockMemoryResource.h"
#include "utils.h"
#include <MultiPrecision/Unsigned.h>
#include <gtest/gtest.h>

namespace MultiPrecision {

TEST(ReduceModuloUnsigned, DividendZero)
{
	{
		Unsigned testee{u({})};
		EXPECT_EQ(&(testee %= u({1})), &testee);
		EXPECT_EQ(testee, u({}));
	}
	{
		Unsigned testee{u({})};
		EXPECT_EQ(&(testee %= u({wMax})), &testee);
		EXPECT_EQ(testee, u({}));
	}
	{
		Unsigned testee{u({})};
		EXPECT_EQ(&(testee %= u({1, wMax - 1})), &testee);
		EXPECT_EQ(testee, u({}));
	}
}

TEST(ReduceModuloUnsigned, DivisionByZero)
{
	{
		Unsigned testee{u({})};
		EXPECT_THROW(testee %= u({}), std::range_error);
	}
	{
		Unsigned testee{u({1})};
		EXPECT_THROW(testee %= u({}), std::range_error);
	}
	{
		Unsigned testee{u({1, wMax - 1})};
		EXPECT_THROW(testee %= u({}), std::range_error);
	}
}

TEST(ReduceModuloUnsigned, SmallWordDivisorNoRemainder)
{
	{
		Unsigned testee{u({1})};
		EXPECT_EQ(&(testee %= u({1})), &testee);
		EXPECT_EQ(testee, u({}));
	}
	{
		Unsigned testee{u({1, 1})};
		EXPECT_EQ(&(testee %= u({1})), &testee);
		EXPECT_EQ(testee, u({}));
	}
	{
		Unsigned testee{u({1, 1})};
		EXPECT_EQ(&(testee %= u({1, 1})), &testee);
		EXPECT_EQ(testee, u({}));
	}
	{
		Unsigned testee{u({1, 2, 2, 1})};
		EXPECT_EQ(&(testee %= u({1, 1})), &testee);
		EXPECT_EQ(testee, u({}));
	}
	{
		Unsigned testee{u({1, 2, 2, 1})};
		EXPECT_EQ(&(testee %= u({1, 1, 1})), &testee);
		EXPECT_EQ(testee, u({}));
	}
	{
		Unsigned testee{u({1, 2, 3, 2, 1})};
		EXPECT_EQ(&(testee %= u({1, 1, 1})), &testee);
		EXPECT_EQ(testee, u({}));
	}
}

TEST(ReduceModuloUnsigned, LargeWordDivisorNoRemainder)
{
	{
		Unsigned testee{u({1, wMax - 1})};
		EXPECT_EQ(&(testee %= u({wMax})), &testee);
		EXPECT_EQ(testee, u({}));
	}
	{
		Unsigned testee{u({1, wMax, wMax - 1})};
		EXPECT_EQ(&(testee %= u({wMax})), &testee);
		EXPECT_EQ(testee, u({}));
	}
	{
		Unsigned testee{u({1, wMax, wMax - 1})};
		EXPECT_EQ(&(testee %= u({wMax, wMax})), &testee);
		EXPECT_EQ(testee, u({}));
	}
	{
		Unsigned testee{u({1, 0, wMax, wMax - 1, wMax})};
		EXPECT_EQ(&(testee %= u({wMax, wMax})), &testee);
		EXPECT_EQ(testee, u({}));
	}
	{
		Unsigned testee{u({1, 0, wMax, wMax - 1, wMax})};
		EXPECT_EQ(&(testee %= u({wMax, wMax, wMax})), &testee);
		EXPECT_EQ(testee, u({}));
	}
	{
		Unsigned testee{u({1, 0, 0, wMax - 1, wMax, wMax})};
		EXPECT_EQ(&(testee %= u({wMax, wMax, wMax})), &testee);
		EXPECT_EQ(testee, u({}));
	}
}

TEST(ReduceModuloUnsigned, SmallWordDivisorRemainder)
{
	{
		Unsigned testee{u({1})};
		EXPECT_EQ(&(testee %= u({2})), &testee);
		EXPECT_EQ(testee, u({1}));
	}
	{
		Unsigned testee{u({1, 1})};
		EXPECT_EQ(&(testee %= u({2})), &testee);
		EXPECT_EQ(testee, u({1}));
	}
	{
		Unsigned testee{u({1, 1})};
		EXPECT_EQ(&(testee %= u({0, 1})), &testee);
		EXPECT_EQ(testee, u({1}));
	}
	{
		Unsigned testee{u({1, 2, 2, 1})};
		EXPECT_EQ(&(testee %= u({0, 1})), &testee);
		EXPECT_EQ(testee, u({1}));
	}
	{
		Unsigned testee{u({1, 2, 2, 1})};
		EXPECT_EQ(&(testee %= u({1, 0, 1})), &testee);
		EXPECT_EQ(testee, u({wMax}));
	}
	{
		Unsigned testee{u({1, 2, 3, 2, 1})};
		EXPECT_EQ(&(testee %= u({1, 0, 1})), &testee);
		EXPECT_EQ(testee, u({0, 0, 1}));
	}
}

TEST(ReduceModuloUnsigned, LargeWordDivisorRemainder)
{
	{
		Unsigned testee{u({1, wMax - 1})};
		EXPECT_EQ(&(testee %= u({wMax - 1})), &testee);
		EXPECT_EQ(testee, u({1}));
	}
	{
		Unsigned testee{u({1, wMax, wMax - 1})};
		EXPECT_EQ(&(testee %= u({wMax - 1})), &testee);
		EXPECT_EQ(testee, u({3}));
	}
	{
		Unsigned testee{u({1, wMax, wMax - 1})};
		EXPECT_EQ(&(testee %= u({wMax - 1, wMax})), &testee);
		EXPECT_EQ(testee, u({wMax}));
	}
	{
		Unsigned testee{u({1, 0, wMax, wMax - 1, wMax})};
		EXPECT_EQ(&(testee %= u({wMax - 1, wMax})), &testee);
		EXPECT_EQ(testee, u({wMax, 1}));
	}
	{
		Unsigned testee{u({1, 0, wMax, wMax - 1, wMax})};
		EXPECT_EQ(&(testee %= u({wMax, wMax - 1, wMax})), &testee);
		EXPECT_EQ(testee, u({1}));
	}
	{
		Unsigned testee{u({1, 0, 0, wMax - 1, wMax, wMax})};
		EXPECT_EQ(&(testee %= u({wMax, wMax - 1, wMax})), &testee);
		EXPECT_EQ(testee, u({0, 0, 1}));
	}
}

TEST(ReduceModuloUnsigned, Allocations)
{
	{
		testing::StrictMock<MockMemoryResource> r1, r2;
		EXPECT_CALL(r1, do_allocate(testing::_, testing::_)).Times(3);
		EXPECT_CALL(r2, do_allocate(testing::_, testing::_)).Times(4);
		Unsigned testee{u({1, 0, 0, wMax - 1, wMax, wMax}, r1)};
		testee %= u({wMax, wMax - 1, wMax}, r2);
	}
	{
		testing::StrictMock<MockMemoryResource> r1, r2;
		EXPECT_CALL(r1, do_allocate(testing::_, testing::_)).Times(2);
		EXPECT_CALL(r2, do_allocate(testing::_, testing::_)).Times(1);
		Unsigned testee{u({1, 0, 0, wMax - 1, wMax, wMax}, r1)};
		testee %= u({wMax - 1}, r2);
	}
}

TEST(ReduceModuloUnsigned, OutOfMemory)
{
	{
		testing::StrictMock<MockMemoryResource> r;
		testing::Sequence s;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(1).InSequence(s);
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
		Unsigned testee{u({1, 0, 0, wMax - 1, wMax, wMax}, r)};
		EXPECT_THROW(testee %= u({wMax, wMax - 1, wMax}), std::bad_alloc);
	}
	{
		testing::StrictMock<MockMemoryResource> r;
		testing::Sequence s;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(2).InSequence(s);
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
		Unsigned testee{u({1, 0, 0, wMax - 1, wMax, wMax}, r)};
		EXPECT_THROW(testee %= u({wMax, wMax - 1, wMax}), std::bad_alloc);
	}
	{
		testing::StrictMock<MockMemoryResource> r;
		testing::Sequence s;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(1).InSequence(s);
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
		Unsigned testee{u({1, 0, 0, wMax - 1, wMax, wMax})};
		EXPECT_THROW(testee %= u({wMax, wMax - 1, wMax}, r), std::bad_alloc);
	}
	{
		testing::StrictMock<MockMemoryResource> r;
		testing::Sequence s;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(2).InSequence(s);
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
		Unsigned testee{u({1, 0, 0, wMax - 1, wMax, wMax})};
		EXPECT_THROW(testee %= u({wMax, wMax - 1, wMax}, r), std::bad_alloc);
	}
	{
		testing::StrictMock<MockMemoryResource> r;
		testing::Sequence s;
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).Times(3).InSequence(s);
		EXPECT_CALL(r, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
		Unsigned testee{u({1, 0, 0, wMax - 1, wMax, wMax})};
		EXPECT_THROW(testee %= u({wMax, wMax - 1, wMax}, r), std::bad_alloc);
	}
}

} // namespace MultiPrecision
