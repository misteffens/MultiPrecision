//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "utils.h"
#include <MultiPrecision/Unsigned.h>
#include <gtest/gtest.h>

namespace MultiPrecision {

TEST(ShiftLeft, ByZero)
{
	Unsigned testee{u({1})};
	EXPECT_EQ(&(testee <<= 0), &testee);
	EXPECT_EQ(testee, u({1}));
}

TEST(ShiftLeft, ByDigits)
{
	{
		Unsigned testee{u({1})};
		EXPECT_EQ(&(testee <<= 1), &testee);
		EXPECT_EQ(testee, u({2}));
	}
	{
		Unsigned testee{u({1})};
		EXPECT_EQ(&(testee <<= (wDigits - 1)), &testee);
		EXPECT_EQ(testee, u({w{1} << (wDigits - 1)}));
	}
}

TEST(ShiftLeft, ByMultipleOfWords)
{
	{
		Unsigned testee{u({1})};
		EXPECT_EQ(&(testee <<= wDigits), &testee);
		EXPECT_EQ(testee, u({0, 1}));
	}
	{
		Unsigned testee{u({1, 1})};
		EXPECT_EQ(&(testee <<= wDigits), &testee);
		EXPECT_EQ(testee, u({0, 1, 1}));
	}
	{
		Unsigned testee{u({1})};
		EXPECT_EQ(&(testee <<= (2 * wDigits)), &testee);
		EXPECT_EQ(testee, u({0, 0, 1}));
	}
	{
		Unsigned testee{u({1, 1})};
		EXPECT_EQ(&(testee <<= (2 * wDigits)), &testee);
		EXPECT_EQ(testee, u({0, 0, 1, 1}));
	}
}

TEST(ShiftLeft, ByMultipleOfWordsAndDigits)
{
	{
		Unsigned testee{u({wMax})};
		EXPECT_EQ(&(testee <<= 1), &testee);
		EXPECT_EQ(testee, u({wMax - 1, 1}));
	}
	{
		Unsigned testee{u({wMax})};
		EXPECT_EQ(&(testee <<= (wDigits + 1)), &testee);
		EXPECT_EQ(testee, u({0, wMax - 1, 1}));
	}
	{
		Unsigned testee{u({1})};
		EXPECT_EQ(&(testee <<= (2 * wDigits - 1)), &testee);
		EXPECT_EQ(testee, u({0, w{1} << (wDigits - 1)}));
	}
}

} // namespace MultiPrecision
