#ifndef FreeListResource_h_INCLUDED
#define FreeListResource_h_INCLUDED

#include <list>
#include <memory_resource>

template<typename WordType, std::size_t wordsPerBlock>
class FreeListResource : public std::pmr::memory_resource
{
public:
	FreeListResource(std::size_t blocks) : free{blocks}
	{
		for (typename std::list<Block>::iterator i{free.begin()}; i != free.end(); ++i) {
			i->id = i;
		}
	}

	void* do_allocate(std::size_t bytes, std::size_t alignment) override
	{
		if (!free.empty() && bytes <= sizeof(WordType) * wordsPerBlock) {
			allocated.splice(allocated.begin(), free, free.begin());
			return &allocated.front().data;
		} else {
			throw std::bad_alloc{};
		}
	}

	void do_deallocate(void* p, std::size_t bytes, std::size_t alignment) override
	{
		Block* block = reinterpret_cast<Block*>(reinterpret_cast<char*>(p) - offsetof(Block, data));
		free.splice(free.begin(), allocated, block->id);
	}

	bool do_is_equal(std::pmr::memory_resource const& other) const noexcept override
	{
		return this == &other;
	}

private:
	struct Block
	{
		typename std::list<Block>::iterator id;
		WordType data[wordsPerBlock];
	};
	std::list<Block> free;
	std::list<Block> allocated;
};

#endif // FreeListResource_h_INCLUDED
