//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "FreeListResource.h"
#include <MultiPrecision/Unsigned.h>
#include <MultiPrecision/charconv.h>
#include <benchmark/benchmark.h>
#include <cstring>
#include <gmp.h>

char const* const dividendStr{
	"123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456"
	"789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012"
	"345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678"
	"90123456789012345678901234567890123456789012345678901234567890"};
char const* const divisorStr{
	"987654321098765432109876543210987654321098765432109876543210987654321098765432109876543210987654321098765432109876543210"};

static void BM_DivUnsigned(benchmark::State& state)
{
	MultiPrecision::Unsigned dividend{std::pmr::polymorphic_allocator<MultiPrecision::Unsigned::WordType>{}};
	MultiPrecision::Unsigned divisor{std::pmr::polymorphic_allocator<MultiPrecision::Unsigned::WordType>{}};
	from_chars(dividendStr, dividendStr + strlen(dividendStr), dividend);
	from_chars(divisorStr, divisorStr + strlen(divisorStr), divisor);
	// MEASUREMENT
	for (auto _ : state) {
		MultiPrecision::Unsigned::DivisionByUnsignedResult result{dividend.div(divisor)};
		benchmark::DoNotOptimize(result);
		benchmark::ClobberMemory();
	}
}

static void BM_DivUnsigned_PoolResource(benchmark::State& state)
{
	std::pmr::unsynchronized_pool_resource memResource{std::pmr::pool_options{1000, 1000}};
	MultiPrecision::Unsigned dividend{std::pmr::polymorphic_allocator<MultiPrecision::Unsigned::WordType>{&memResource}};
	MultiPrecision::Unsigned divisor{std::pmr::polymorphic_allocator<MultiPrecision::Unsigned::WordType>{&memResource}};
	from_chars(dividendStr, dividendStr + strlen(dividendStr), dividend);
	from_chars(divisorStr, divisorStr + strlen(divisorStr), divisor);
	// MEASUREMENT
	for (auto _ : state) {
		MultiPrecision::Unsigned::DivisionByUnsignedResult result{dividend.div(divisor)};
		benchmark::DoNotOptimize(result);
		benchmark::ClobberMemory();
	}
}

static void BM_DivUnsigned_FreeListResource(benchmark::State& state)
{
	FreeListResource<MultiPrecision::Unsigned::WordType, 64> resource{8};
	MultiPrecision::Unsigned dividend{std::pmr::polymorphic_allocator<MultiPrecision::Unsigned::WordType>{&resource}};
	MultiPrecision::Unsigned divisor{std::pmr::polymorphic_allocator<MultiPrecision::Unsigned::WordType>{&resource}};
	from_chars(dividendStr, dividendStr + strlen(dividendStr), dividend);
	from_chars(divisorStr, divisorStr + strlen(divisorStr), divisor);
	// MEASUREMENT
	for (auto _ : state) {
		MultiPrecision::Unsigned::DivisionByUnsignedResult result{dividend.div(divisor)};
		benchmark::DoNotOptimize(result);
		benchmark::ClobberMemory();
	}
}

static void BM_DivUnsigned_GMP(benchmark::State& state)
{
	mpz_t dividend, divisor, quotient, remainder;
	mpz_init_set_str(dividend, dividendStr, 10);
	mpz_init_set_str(divisor, divisorStr, 10);
	// MEASUREMENT
	for (auto _ : state) {
		mpz_init(quotient);
		mpz_init(remainder);
		mpz_tdiv_qr(quotient, remainder, dividend, divisor);
		benchmark::DoNotOptimize(quotient);
		benchmark::DoNotOptimize(remainder);
		benchmark::ClobberMemory();
		mpz_clear(quotient);
		mpz_clear(remainder);
	}
}


BENCHMARK(BM_DivUnsigned);
BENCHMARK(BM_DivUnsigned_PoolResource);
BENCHMARK(BM_DivUnsigned_FreeListResource);
BENCHMARK(BM_DivUnsigned_GMP);
