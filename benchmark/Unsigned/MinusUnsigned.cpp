//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "FreeListResource.h"
#include <MultiPrecision/Unsigned.h>
#include <MultiPrecision/charconv.h>
#include <benchmark/benchmark.h>
#include <cstring>
#include <gmp.h>

char const* const minuendStr{
	"123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456"
	"789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012"
	"345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678"
	"90123456789012345678901234567890123456789012345678901234567890"};
char const* const subtrahendStr{
	"987654321098765432109876543210987654321098765432109876543210987654321098765432109876543210987654321098765432109876543210"};

static void BM_MinusUnsigned(benchmark::State& state)
{
	MultiPrecision::Unsigned minuend{std::pmr::polymorphic_allocator<MultiPrecision::Unsigned::WordType>{}};
	MultiPrecision::Unsigned subtrahend{std::pmr::polymorphic_allocator<MultiPrecision::Unsigned::WordType>{}};
	from_chars(minuendStr, minuendStr + strlen(minuendStr), minuend);
	from_chars(subtrahendStr, subtrahendStr + strlen(subtrahendStr), subtrahend);
	// MEASUREMENT
	for (auto _ : state) {
		MultiPrecision::Unsigned difference{minuend - subtrahend};
		benchmark::DoNotOptimize(difference);
		benchmark::ClobberMemory();
	}
}

static void BM_MinusUnsigned_PoolResource(benchmark::State& state)
{
	std::pmr::unsynchronized_pool_resource memResource{std::pmr::pool_options{1000, 1000}};
	MultiPrecision::Unsigned minuend{std::pmr::polymorphic_allocator<MultiPrecision::Unsigned::WordType>{&memResource}};
	MultiPrecision::Unsigned subtrahend{std::pmr::polymorphic_allocator<MultiPrecision::Unsigned::WordType>{&memResource}};
	from_chars(minuendStr, minuendStr + strlen(minuendStr), minuend);
	from_chars(subtrahendStr, subtrahendStr + strlen(subtrahendStr), subtrahend);
	// MEASUREMENT
	for (auto _ : state) {
		MultiPrecision::Unsigned difference{minuend - subtrahend};
		benchmark::DoNotOptimize(difference);
		benchmark::ClobberMemory();
	}
}

static void BM_MinusUnsigned_FreeListResource(benchmark::State& state)
{
	FreeListResource<MultiPrecision::Unsigned::WordType, 64> resource{8};
	MultiPrecision::Unsigned minuend{std::pmr::polymorphic_allocator<MultiPrecision::Unsigned::WordType>{&resource}};
	MultiPrecision::Unsigned subtrahend{std::pmr::polymorphic_allocator<MultiPrecision::Unsigned::WordType>{&resource}};
	from_chars(minuendStr, minuendStr + strlen(minuendStr), minuend);
	from_chars(subtrahendStr, subtrahendStr + strlen(subtrahendStr), subtrahend);
	// MEASUREMENT
	for (auto _ : state) {
		MultiPrecision::Unsigned difference{minuend - subtrahend};
		benchmark::DoNotOptimize(difference);
		benchmark::ClobberMemory();
	}
}

static void BM_MinusUnsigned_GMP(benchmark::State& state)
{
	mpz_t minuend, subtrahend, difference;
	mpz_init_set_str(minuend, minuendStr, 10);
	mpz_init_set_str(subtrahend, subtrahendStr, 10);
	// MEASUREMENT
	for (auto _ : state) {
		mpz_init(difference);
		mpz_sub(difference, minuend, subtrahend);
		benchmark::DoNotOptimize(difference);
		benchmark::ClobberMemory();
		mpz_clear(difference);
	}
}

BENCHMARK(BM_MinusUnsigned);
BENCHMARK(BM_MinusUnsigned_PoolResource);
BENCHMARK(BM_MinusUnsigned_FreeListResource);
BENCHMARK(BM_MinusUnsigned_GMP);
