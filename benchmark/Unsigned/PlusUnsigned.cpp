//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "FreeListResource.h"
#include <MultiPrecision/Unsigned.h>
#include <MultiPrecision/charconv.h>
#include <benchmark/benchmark.h>
#include <cstring>
#include <gmp.h>

char const* const augendStr{
	"123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456"
	"789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012"
	"345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678"
	"90123456789012345678901234567890123456789012345678901234567890"};
char const* const addendStr{
	"987654321098765432109876543210987654321098765432109876543210987654321098765432109876543210987654321098765432109876543210"};

static void BM_PlusUnsigned(benchmark::State& state)
{
	MultiPrecision::Unsigned augend{std::pmr::polymorphic_allocator<MultiPrecision::Unsigned::WordType>{}};
	MultiPrecision::Unsigned addend{std::pmr::polymorphic_allocator<MultiPrecision::Unsigned::WordType>{}};
	from_chars(augendStr, augendStr + strlen(augendStr), augend);
	from_chars(addendStr, addendStr + strlen(addendStr), addend);
	// MEASUREMENT
	for (auto _ : state) {
		MultiPrecision::Unsigned sum{augend + addend};
		benchmark::DoNotOptimize(sum);
		benchmark::ClobberMemory();
	}
}

static void BM_PlusUnsigned_PoolResource(benchmark::State& state)
{
	std::pmr::unsynchronized_pool_resource memResource{std::pmr::pool_options{1000, 1000}};
	MultiPrecision::Unsigned augend{std::pmr::polymorphic_allocator<MultiPrecision::Unsigned::WordType>{&memResource}};
	MultiPrecision::Unsigned addend{std::pmr::polymorphic_allocator<MultiPrecision::Unsigned::WordType>{&memResource}};
	from_chars(augendStr, augendStr + strlen(augendStr), augend);
	from_chars(addendStr, addendStr + strlen(addendStr), addend);
	// MEASUREMENT
	for (auto _ : state) {
		MultiPrecision::Unsigned sum{augend + addend};
		benchmark::DoNotOptimize(sum);
		benchmark::ClobberMemory();
	}
}

static void BM_PlusUnsigned_FreeListResource(benchmark::State& state)
{
	FreeListResource<MultiPrecision::Unsigned::WordType, 64> resource{8};
	MultiPrecision::Unsigned augend{std::pmr::polymorphic_allocator<MultiPrecision::Unsigned::WordType>{&resource}};
	MultiPrecision::Unsigned addend{std::pmr::polymorphic_allocator<MultiPrecision::Unsigned::WordType>{&resource}};
	from_chars(augendStr, augendStr + strlen(augendStr), augend);
	from_chars(addendStr, addendStr + strlen(addendStr), addend);
	// MEASUREMENT
	for (auto _ : state) {
		MultiPrecision::Unsigned sum{augend + addend};
		benchmark::DoNotOptimize(sum);
		benchmark::ClobberMemory();
	}
}

static void BM_PlusUnsigned_GMP(benchmark::State& state)
{
	mpz_t augend, addend, sum;
	mpz_init_set_str(augend, augendStr, 10);
	mpz_init_set_str(addend, addendStr, 10);
	// MEASUREMENT
	for (auto _ : state) {
		mpz_init(sum);
		mpz_add(sum, augend, addend);
		benchmark::DoNotOptimize(sum);
		benchmark::ClobberMemory();
		mpz_clear(sum);
	}
}

BENCHMARK(BM_PlusUnsigned);
BENCHMARK(BM_PlusUnsigned_PoolResource);
BENCHMARK(BM_PlusUnsigned_FreeListResource);
BENCHMARK(BM_PlusUnsigned_GMP);
