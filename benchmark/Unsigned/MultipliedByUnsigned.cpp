//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "FreeListResource.h"
#include <MultiPrecision/Unsigned.h>
#include <MultiPrecision/charconv.h>
#include <benchmark/benchmark.h>
#include <cstring>
#include <gmp.h>

char const* const multiplicandStr{
	"123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456"
	"789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012"
	"345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678"
	"90123456789012345678901234567890123456789012345678901234567890"};
char const* const multiplierStr{
	"987654321098765432109876543210987654321098765432109876543210987654321098765432109876543210987654321098765432109876543210"};

static void BM_MultipliedByUnsigned(benchmark::State& state)
{
	MultiPrecision::Unsigned multiplicand{std::pmr::polymorphic_allocator<MultiPrecision::Unsigned::WordType>{}};
	MultiPrecision::Unsigned multiplier{std::pmr::polymorphic_allocator<MultiPrecision::Unsigned::WordType>{}};
	from_chars(multiplicandStr, multiplicandStr + strlen(multiplicandStr), multiplicand);
	from_chars(multiplierStr, multiplierStr + strlen(multiplierStr), multiplier);
	// MEASUREMENT
	for (auto _ : state) {
		MultiPrecision::Unsigned product{multiplicand * multiplier};
		benchmark::DoNotOptimize(product);
		benchmark::ClobberMemory();
	}
}

static void BM_MultipliedByUnsigned_PoolResource(benchmark::State& state)
{
	std::pmr::unsynchronized_pool_resource memResource{std::pmr::pool_options{1000, 1000}};
	MultiPrecision::Unsigned multiplicand{std::pmr::polymorphic_allocator<MultiPrecision::Unsigned::WordType>{&memResource}};
	MultiPrecision::Unsigned multiplier{std::pmr::polymorphic_allocator<MultiPrecision::Unsigned::WordType>{&memResource}};
	from_chars(multiplicandStr, multiplicandStr + strlen(multiplicandStr), multiplicand);
	from_chars(multiplierStr, multiplierStr + strlen(multiplierStr), multiplier);
	// MEASUREMENT
	for (auto _ : state) {
		MultiPrecision::Unsigned product{multiplicand * multiplier};
		benchmark::DoNotOptimize(product);
		benchmark::ClobberMemory();
	}
}

static void BM_MultipliedByUnsigned_FreeListResource(benchmark::State& state)
{
	FreeListResource<MultiPrecision::Unsigned::WordType, 64> resource{8};
	MultiPrecision::Unsigned multiplicand{std::pmr::polymorphic_allocator<MultiPrecision::Unsigned::WordType>{&resource}};
	MultiPrecision::Unsigned multiplier{std::pmr::polymorphic_allocator<MultiPrecision::Unsigned::WordType>{&resource}};
	from_chars(multiplicandStr, multiplicandStr + strlen(multiplicandStr), multiplicand);
	from_chars(multiplierStr, multiplierStr + strlen(multiplierStr), multiplier);
	// MEASUREMENT
	for (auto _ : state) {
		MultiPrecision::Unsigned product{multiplicand * multiplier};
		benchmark::DoNotOptimize(product);
		benchmark::ClobberMemory();
	}
}

static void BM_MultipliedByUnsigned_GMP(benchmark::State& state)
{
	mpz_t multiplicand, multiplier, product;
	mpz_init_set_str(multiplicand, multiplicandStr, 10);
	mpz_init_set_str(multiplier, multiplierStr, 10);
	// MEASUREMENT
	for (auto _ : state) {
		mpz_init(product);
		mpz_mul(product, multiplicand, multiplier);
		benchmark::DoNotOptimize(product);
		benchmark::ClobberMemory();
		mpz_clear(product);
	}
}

BENCHMARK(BM_MultipliedByUnsigned);
BENCHMARK(BM_MultipliedByUnsigned_PoolResource);
BENCHMARK(BM_MultipliedByUnsigned_FreeListResource);
BENCHMARK(BM_MultipliedByUnsigned_GMP);
