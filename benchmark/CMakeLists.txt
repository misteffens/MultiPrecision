#
# Copyright (C) 2023 Dr. Michael Steffens
#
# SPDX-License-Identifier:      BSL-1.0
#

find_package(benchmark REQUIRED)
find_package(Threads REQUIRED)
find_package(GMP REQUIRED)

file(GLOB UnsignedBenchmark_SRCS_G Unsigned/*.cpp)
add_executable(benchmarkUnsigned ${UnsignedBenchmark_SRCS_G})
target_include_directories(benchmarkUnsigned PRIVATE $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>)
target_link_libraries(benchmarkUnsigned MultiPrecision benchmark Threads::Threads GMP::GMP)
