//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "MultiPrecision/Unsigned.h"
#include "MultiPrecision/WordTypes.h"
#include <array>

namespace MultiPrecision {

std::string Unsigned::toHexString() const
{
	static constexpr std::array<char, 16> hexChar{{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'}};
	std::string result;
	result.reserve(magnitude.size() * sizeof(WordType) * 2);
	bool inMagnitudeDigits{false};
	for (decltype(magnitude)::const_reverse_iterator w{magnitude.rbegin()}; w != magnitude.rend(); ++w) {
		for (int i{0}; i < std::numeric_limits<WordType>::digits; i += 4) {
			int hexDigit{int(*w >> (std::numeric_limits<WordType>::digits - 4 - i)) & 0xF};
			if (hexDigit != 0) {
				inMagnitudeDigits = true;
			}
			if (inMagnitudeDigits) {
				result.push_back(hexChar[hexDigit]);
			}
		}
	}
	if (!inMagnitudeDigits) {
		result.push_back(hexChar[0]);
	}
	return result;
}

bool Unsigned::operator==(Unsigned const& other) const noexcept
{
	return magnitude == other.magnitude;
}

bool Unsigned::operator<(Unsigned const& other) const noexcept
{
	return magnitude.size() < other.magnitude.size() ||
		   magnitude.size() == other.magnitude.size() &&
			   std::lexicographical_compare(magnitude.rbegin(), magnitude.rend(), other.magnitude.rbegin(), other.magnitude.rend());
}

bool Unsigned::operator==(WordType other) const noexcept
{
	return other == 0 ? magnitude.empty() : magnitude.size() == 1 && magnitude.front() == other;
}

bool Unsigned::operator<(WordType other) const noexcept
{
	return other != 0 && (magnitude.empty() || magnitude.size() == 1 && magnitude.front() < other);
}

bool Unsigned::operator>(WordType other) const noexcept
{
	return other == 0 ? !magnitude.empty() : magnitude.size() == 1 && magnitude.front() > other || magnitude.size() > 1;
}

void Unsigned::trim() noexcept
{
	while (!magnitude.empty() && magnitude.back() == 0) {
		magnitude.pop_back();
	}
}

std::size_t Unsigned::digits() const noexcept
{
	std::size_t result = magnitude.size() * std::numeric_limits<WordType>::digits;
	if (result) {
		for (WordType mask{WordType(1) << (std::numeric_limits<WordType>::digits - 1)}; mask && (magnitude.back() & mask) == 0;
			 mask >>= 1) {
			--result;
		}
	}
	return result;
}

std::ostream& operator<<(std::ostream& os, Unsigned const& n)
{
	os << n.toHexString();
	return os;
}

} // namespace MultiPrecision
