//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "MultiPrecision/charconv.h"
#include "MultiPrecision/Unsigned.h"

namespace MultiPrecision {

namespace {

class UnsignedToCharsConversion
{
public:
	UnsignedToCharsConversion(char* first, char* last, Unsigned const& value, int base) noexcept :
		last{last}, base{base}, divisionResult{value, 0}, result{first, std::errc{}}
	{
	}

	std::to_chars_result operator()() noexcept
	{
		if (base > 1 && base <= 36) {
			leastSignificantDigit();
		} else {
			result.ec = std::errc::invalid_argument;
		}
		return result;
	}

private:
	void leastSignificantDigit() noexcept
	{
		divisionResult = divisionResult.quotient.div(base);
		const int digit(divisionResult.remainder);
		if (divisionResult.quotient != 0) {
			leastSignificantDigit();
		}
		if (result.ptr != last) {
			*result.ptr++ = (digit < 10 ? '0' + digit : 'a' + (digit - 10));
		} else {
			result.ec = std::errc::value_too_large;
		}
	}

	char* last;
	int base;
	Unsigned::DivisionByWordTypeResult divisionResult;
	std::to_chars_result result;
};

int fromChar(char c)
{
	return c >= '0' && c <= '9' ? c - '0' : c >= 'a' && c <= 'z' ? c - 'a' + 10 : c >= 'A' && c <= 'Z' ? c - 'A' + 10 : -1;
}

} // namespace

std::to_chars_result to_chars(char* first, char* last, Unsigned const& value, int base) noexcept
{
	return UnsignedToCharsConversion(first, last, value, base)();
}

std::from_chars_result from_chars(char const* first, char const* last, Unsigned& value, int base) noexcept
{
	std::from_chars_result result{first, std::errc::invalid_argument};
	if (base > 1 && base <= 36) {
		int digit{fromChar(*first)};
		if (digit >= 0 && digit < base) {
			value = WordType(digit);
			result.ec = std::errc{};
			while (++result.ptr != last) {
				digit = fromChar(*result.ptr);
				if (digit >= 0 && digit < base) {
					value *= base;
					value += WordType(digit);
				} else {
					break;
				}
			}
		}
	}
	return result;
}

} // namespace MultiPrecision
