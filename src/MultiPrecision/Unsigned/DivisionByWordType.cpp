//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "MultiPrecision/Unsigned.h"
#include "MultiPrecision/WordTypes.h"

namespace MultiPrecision {

/// Simplified algorithm D for n == 1 from Donald E. Knuth, "The Art of Computer Programming", 2nd vol, 3rd ed, pp 272 and p 625.
class Unsigned::DivisionByWordType
{
public:
	DivisionByWordType(Unsigned const& dividend, WordType divisor) :
		dividend{dividend}, divisor{divisor}, quotient{dividend.magnitude.get_allocator()}, remainder{0}
	{
	}

	Unsigned::DivisionByWordTypeResult computeQuotientAndRemainder()
	{
		iterateQuotientWords();
		quotient.trim();
		return Unsigned::DivisionByWordTypeResult{std::move(quotient), remainder};
	}

	Unsigned computeQuotient()
	{
		iterateQuotientWords();
		quotient.trim();
		return std::move(quotient);
	}

private:
	void iterateQuotientWords()
	{
		quotient.magnitude.resize(dividend.magnitude.size());
		for (std::size_t i{dividend.magnitude.size()}; i != 0; --i) {
			const DoubleWordType tmp{
				(DoubleWordType(remainder) << std::numeric_limits<WordType>::digits) + dividend.magnitude[i - 1]};
			quotient.magnitude[i - 1] = WordType(tmp / divisor);
			remainder = WordType(tmp % divisor);
		}
		quotient.trim();
	}

	Unsigned const& dividend;
	const WordType divisor;
	Unsigned quotient;
	WordType remainder;
};

Unsigned Unsigned::operator/(WordType divisor) const
{
	if (divisor != 0) {
		return DivisionByWordType(*this, divisor).computeQuotient();
	} else {
		throw std::range_error("MultiPrecision::Unsigned::operator/(WordType): division by zero.");
	}
}

Unsigned& Unsigned::operator/=(WordType divisor)
{
	if (divisor != 0) {
		*this = DivisionByWordType(*this, divisor).computeQuotient();
		return *this;
	} else {
		throw std::range_error("MultiPrecision::Unsigned::operator/=(WordType): division by zero.");
	}
}

Unsigned::DivisionByWordTypeResult Unsigned::div(WordType divisor) const
{
	if (divisor != 0) {
		return DivisionByWordType(*this, divisor).computeQuotientAndRemainder();
	} else {
		throw std::range_error("MultiPrecision::Unsigned::div(WordType): division by zero.");
	}
}

} // namespace MultiPrecision
