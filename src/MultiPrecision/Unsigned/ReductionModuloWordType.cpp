//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "MultiPrecision/Unsigned.h"
#include "MultiPrecision/WordTypes.h"

namespace MultiPrecision {

/// Simplified algorithm D for n == 1 from Donald E. Knuth, "The Art of Computer Programming", 2nd vol, 3rd ed, pp 272 and p 625.
class Unsigned::ReductionModuloWordType
{
public:
	ReductionModuloWordType(Unsigned const& dividend, WordType divisor) : dividend{dividend}, divisor{divisor}, remainder{0}
	{
	}

	WordType computeRemainder()
	{
		iterateQuotientWords();
		return remainder;
	}

private:
	void iterateQuotientWords()
	{
		for (std::size_t i{dividend.magnitude.size()}; i != 0; --i) {
			const DoubleWordType tmp{
				(DoubleWordType(remainder) << std::numeric_limits<WordType>::digits) + dividend.magnitude[i - 1]};
			remainder = WordType(tmp % divisor);
		}
	}

	Unsigned const& dividend;
	const WordType divisor;
	WordType remainder;
};

Unsigned Unsigned::operator%(WordType divisor) const
{
	if (divisor != 0) {
		return Unsigned{ReductionModuloWordType(*this, divisor).computeRemainder(), magnitude.get_allocator()};
	} else {
		throw std::range_error("MultiPrecision::Unsigned::operator%(WordType): division by zero.");
	}
}

Unsigned& Unsigned::operator%=(WordType divisor)
{
	if (divisor != 0) {
		*this = Unsigned{ReductionModuloWordType(*this, divisor).computeRemainder(), magnitude.get_allocator()};
		return *this;
	} else {
		throw std::range_error("MultiPrecision::Unsigned::operator%=(WordType): division by zero.");
	}
}

} // namespace MultiPrecision
