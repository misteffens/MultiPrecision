//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "MultiPrecision/Unsigned.h"
#include "MultiPrecision/WordTypes.h"

namespace MultiPrecision {

class Unsigned::RightShift
{
public:
	RightShift(const decltype(Unsigned::magnitude)& magnitude, decltype(Unsigned::magnitude)& result, std::size_t digits) :
		magnitude(magnitude),
		result(result),
		distanceWords(digits / std::numeric_limits<WordType>::digits),
		distanceDigits(digits % std::numeric_limits<WordType>::digits)
	{
	}

	template<bool inPlace>
	void shiftDigits()
	{
		if (distanceWords < magnitude.size()) {
			if (!inPlace) {
				result.resize(magnitude.size() - distanceWords);
			}
			for (std::size_t i{0}; i < magnitude.size() - distanceWords - 1; ++i) {
				result[i] = leftDigitsOf(magnitude[i + distanceWords]) | rightDigitsOf(magnitude[i + distanceWords + 1]);
			}
			result[magnitude.size() - distanceWords - 1] = leftDigitsOf(magnitude[magnitude.size() - 1]);
			if (inPlace) {
				result.resize(magnitude.size() - distanceWords);
			}
			if (result.back() == 0) {
				result.pop_back();
			}
		} else {
			result.clear();
		}
	}

private:
	WordType rightDigitsOf(WordType word)
	{
		return distanceDigits ? word << (std::numeric_limits<WordType>::digits - distanceDigits) : 0;
	}

	WordType leftDigitsOf(WordType word)
	{
		return word >> distanceDigits;
	}

	const decltype(Unsigned::magnitude)& magnitude;
	decltype(Unsigned::magnitude)& result;
	const std::size_t distanceWords;
	const std::size_t distanceDigits;
};

Unsigned Unsigned::operator>>(std::size_t digits) const
{
	Unsigned result{magnitude.get_allocator()};
	Unsigned::RightShift(magnitude, result.magnitude, digits).shiftDigits<false>();
	return result;
}

Unsigned& Unsigned::operator>>=(std::size_t digits)
{
	RightShift(magnitude, magnitude, digits).shiftDigits<true>();
	return *this;
}

} // namespace MultiPrecision
