//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "MultiPrecision/Unsigned.h"
#include "MultiPrecision/WordTypes.h"

namespace MultiPrecision {

class Unsigned::SubtractionOfWordType
{
public:
	SubtractionOfWordType(Unsigned const& minuend, WordType subtrahend) :
		minuend{minuend}, subtrahend{subtrahend}, minuendSize{minuend.magnitude.size()}
	{
	}

	void computeDifference(Unsigned& difference)
	{
		if (minuendSize == 0 && subtrahend > 0) {
			throw std::range_error("Unsigned::SubtractionOfWordType::computeDiffererence(Unsigned&): Overflow.");
		}
		if (minuendSize > 0) {
			difference.magnitude.resize(minuendSize);
			const DoubleWordType tmp{minuend.magnitude[0] - DoubleWordType{subtrahend}};
			difference.magnitude[0] = tmp & std::numeric_limits<WordType>::max();
			DoubleWordType borrow{tmp >> std::numeric_limits<WordType>::digits ? 1U : 0};
			std::size_t i{1};
			while (i < minuendSize) {
				const DoubleWordType tmp{minuend.magnitude[i] - borrow};
				difference.magnitude[i] = tmp & std::numeric_limits<WordType>::max();
				borrow = tmp >> std::numeric_limits<WordType>::digits ? 1U : 0;
				++i;
			}
			if (borrow != 0) {
				// TODO: restore magnitude for strong exception guarantee.
				throw std::range_error("Unsigned::SubtractionOfWordType::computeDiffererence(Unsigned&): Overflow.");
			}
			difference.trim();
		}
	}

private:
	Unsigned const& minuend;
	WordType subtrahend;
	const std::size_t minuendSize;
};

Unsigned Unsigned::operator-(WordType subtrahend) const
{
	Unsigned difference{magnitude.get_allocator()};
	SubtractionOfWordType(*this, subtrahend).computeDifference(difference);
	return difference;
}

Unsigned& Unsigned::operator-=(WordType subtrahend)
{
	SubtractionOfWordType(*this, subtrahend).computeDifference(*this);
	return *this;
}

} // namespace MultiPrecision
