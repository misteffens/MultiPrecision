//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "MultiPrecision/Unsigned.h"
#include "MultiPrecision/WordTypes.h"

namespace MultiPrecision {

class Unsigned::SubtractionOfUnsigned
{
public:
	SubtractionOfUnsigned(Unsigned const& minuend, Unsigned const& subtrahend) :
		minuend{minuend}, subtrahend{subtrahend}, minuendSize{minuend.magnitude.size()}
	{
	}

	void computeDifference(Unsigned& difference)
	{
		if (minuendSize < subtrahend.magnitude.size()) {
			throw std::range_error("Unsigned::SubtractionOfUnsigned::computeDiffererence(Unsigned&): Overflow.");
		}
		difference.magnitude.resize(minuendSize);
		DoubleWordType borrow{0};
		std::size_t i{0};
		while (i < subtrahend.magnitude.size()) {
			const DoubleWordType tmp{minuend.magnitude[i] - (subtrahend.magnitude[i] + borrow)};
			difference.magnitude[i] = tmp & std::numeric_limits<WordType>::max();
			borrow = tmp >> std::numeric_limits<WordType>::digits ? 1U : 0;
			++i;
		}
		while (i < minuendSize) {
			const DoubleWordType tmp{minuend.magnitude[i] - borrow};
			difference.magnitude[i] = tmp & std::numeric_limits<WordType>::max();
			borrow = tmp >> std::numeric_limits<WordType>::digits ? 1U : 0;
			++i;
		}
		if (borrow != 0) {
			// TODO: restore magnitude for strong exception guarantee.
			throw std::range_error("Unsigned::SubtractionOfUnsigned::computeDiffererence(Unsigned&): Overflow.");
		}
		difference.trim();
	}

private:
	Unsigned const& minuend;
	Unsigned const& subtrahend;
	const std::size_t minuendSize;
};

Unsigned Unsigned::operator-(Unsigned const& subtrahend) const
{
	Unsigned difference{magnitude.get_allocator()};
	SubtractionOfUnsigned(*this, subtrahend).computeDifference(difference);
	return difference;
}

Unsigned& Unsigned::operator-=(Unsigned const& subtrahend)
{
	SubtractionOfUnsigned(*this, subtrahend).computeDifference(*this);
	return *this;
}

} // namespace MultiPrecision
