//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "MultiPrecision/Unsigned.h"
#include "MultiPrecision/WordTypes.h"

namespace MultiPrecision {

class Unsigned::MultiplicationByUnsigned
{
public:
	MultiplicationByUnsigned(Unsigned const& multiplicand, Unsigned const& multiplier) :
		multiplicand{multiplicand}, multiplier{multiplier}, product{multiplicand.magnitude.get_allocator()}
	{
	}

	Unsigned computeProduct()
	{
		product.magnitude.resize(multiplicand.magnitude.size() + multiplier.magnitude.size());
		for (std::size_t j{0}; j < multiplier.magnitude.size(); ++j) {
			DoubleWordType carry{0};
			for (std::size_t i{0}; i < multiplicand.magnitude.size(); ++i) {
				const DoubleWordType tmp{
					DoubleWordType(multiplicand.magnitude[i]) * multiplier.magnitude[j] + product.magnitude[i + j] + carry};
				product.magnitude[i + j] = tmp & std::numeric_limits<WordType>::max();
				carry = tmp >> std::numeric_limits<WordType>::digits;
			}
			product.magnitude[multiplicand.magnitude.size() + j] = carry;
		}
		product.trim();
		return std::move(product);
	}

private:
	Unsigned const& multiplicand;
	Unsigned const& multiplier;
	Unsigned product;
};

Unsigned Unsigned::operator*(Unsigned const& multiplier) const
{
	return MultiplicationByUnsigned{*this, multiplier}.computeProduct();
}

Unsigned& Unsigned::operator*=(Unsigned const& multiplier)
{
	*this = std::move(MultiplicationByUnsigned{*this, multiplier}.computeProduct());
	return *this;
}

} // namespace MultiPrecision
