//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "MultiPrecision/Unsigned.h"
#include "MultiPrecision/WordTypes.h"

namespace MultiPrecision {

class Unsigned::AdditionOfWordType
{
public:
	AdditionOfWordType(Unsigned const& augend, WordType addend) :
		augend{augend}, addend{addend}, augendSize{augend.magnitude.size()}
	{
	}

	void computeSum(Unsigned& sum)
	{
		sum.magnitude.resize(std::max(augendSize, std::size_t{1}) + 1);
		if (augendSize == 0) {
			sum.magnitude[0] = addend;
		} else {
			const DoubleWordType tmp{augend.magnitude[0] + DoubleWordType{addend}};
			sum.magnitude[0] = tmp & std::numeric_limits<WordType>::max();
			DoubleWordType carry{tmp >> std::numeric_limits<WordType>::digits ? 1U : 0};
			std::size_t i{1};
			while (i < augendSize) {
				const DoubleWordType tmp{carry + augend.magnitude[i]};
				sum.magnitude[i] = tmp & std::numeric_limits<WordType>::max();
				carry = tmp >> std::numeric_limits<WordType>::digits ? 1U : 0;
				++i;
			}
			if (carry) {
				sum.magnitude[i] = carry & std::numeric_limits<WordType>::max();
			}
		}
		sum.trim();
	}

private:
	Unsigned const& augend;
	WordType addend;
	const std::size_t augendSize;
};

Unsigned Unsigned::operator+(WordType addend) const
{
	Unsigned sum{magnitude.get_allocator()};
	AdditionOfWordType(*this, addend).computeSum(sum);
	return sum;
}

Unsigned& Unsigned::operator+=(WordType addend)
{
	AdditionOfWordType(*this, addend).computeSum(*this);
	return *this;
}

} // namespace MultiPrecision
