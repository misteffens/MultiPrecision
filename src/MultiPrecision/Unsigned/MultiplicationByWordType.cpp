//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "MultiPrecision/Unsigned.h"
#include "MultiPrecision/WordTypes.h"

namespace MultiPrecision {

class Unsigned::MultiplicationByWordType
{
public:
	MultiplicationByWordType(Unsigned const& multiplicand, WordType multiplier) :
		multiplicand{multiplicand}, multiplier{multiplier}, product{multiplicand.magnitude.get_allocator()}
	{
	}

	Unsigned computeProduct()
	{
		if (multiplier != 0) {
			product.magnitude.resize(multiplicand.magnitude.size() + 1);
			DoubleWordType carry{0};
			for (std::size_t i{0}; i < multiplicand.magnitude.size(); ++i) {
				const DoubleWordType tmp{DoubleWordType(multiplicand.magnitude[i]) * multiplier + product.magnitude[i] + carry};
				product.magnitude[i] = tmp & std::numeric_limits<WordType>::max();
				carry = tmp >> std::numeric_limits<WordType>::digits;
			}
			product.magnitude[multiplicand.magnitude.size()] = carry;
			product.trim();
		}
		return std::move(product);
	}

private:
	Unsigned const& multiplicand;
	const WordType multiplier;
	Unsigned product;
};

Unsigned Unsigned::operator*(WordType multiplier) const
{
	return MultiplicationByWordType{*this, multiplier}.computeProduct();
}

Unsigned& Unsigned::operator*=(WordType multiplier)
{
	*this = std::move(MultiplicationByWordType{*this, multiplier}.computeProduct());
	return *this;
}

} // namespace MultiPrecision
