//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "MultiPrecision/Unsigned.h"
#include "MultiPrecision/WordTypes.h"

namespace MultiPrecision {

class Unsigned::AdditionOfUnsigned
{
public:
	AdditionOfUnsigned(Unsigned const& augend, Unsigned const& addend) :
		augend{augend}, addend{addend}, augendSize{augend.magnitude.size()}
	{
	}

	void computeSum(Unsigned& sum)
	{
		sum.magnitude.resize(std::max(augendSize, addend.magnitude.size()) + 1);
		DoubleWordType carry{0};
		std::size_t i{0};
		while (i < std::min(augendSize, addend.magnitude.size())) {
			const DoubleWordType tmp{carry + augend.magnitude[i] + addend.magnitude[i]};
			sum.magnitude[i] = tmp & std::numeric_limits<WordType>::max();
			carry = tmp >> std::numeric_limits<WordType>::digits ? 1U : 0;
			++i;
		}
		while (i < augendSize) {
			const DoubleWordType tmp{carry + augend.magnitude[i]};
			sum.magnitude[i] = tmp & std::numeric_limits<WordType>::max();
			carry = tmp >> std::numeric_limits<WordType>::digits ? 1U : 0;
			++i;
		}
		while (i < addend.magnitude.size()) {
			const DoubleWordType tmp{carry + addend.magnitude[i]};
			sum.magnitude[i] = tmp & std::numeric_limits<WordType>::max();
			carry = tmp >> std::numeric_limits<WordType>::digits ? 1U : 0;
			++i;
		}
		if (carry) {
			sum.magnitude[i] = carry & std::numeric_limits<WordType>::max();
		}
		sum.trim();
	}

private:
	Unsigned const& augend;
	Unsigned const& addend;
	const std::size_t augendSize;
};

Unsigned Unsigned::operator+(Unsigned const& addend) const
{
	Unsigned sum{magnitude.get_allocator()};
	AdditionOfUnsigned(*this, addend).computeSum(sum);
	return sum;
}

Unsigned& Unsigned::operator+=(Unsigned const& addend)
{
	AdditionOfUnsigned(*this, addend).computeSum(*this);
	return *this;
}

} // namespace MultiPrecision
