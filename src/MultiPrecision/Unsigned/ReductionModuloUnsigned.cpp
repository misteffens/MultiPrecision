//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "MultiPrecision/Unsigned.h"
#include "MultiPrecision/WordTypes.h"

namespace MultiPrecision {

/// Algorithm D from Donald E. Knuth, "The Art of Computer Programming", 2nd vol, 3rd ed, pp 272.
class Unsigned::ReductionModuloUnsigned
{
public:
	// Expand remainder (initialized by dividend) and divisor by a common factor power of 2, ensuring that the divisor most
	// significant digit is >= RADIX / 2.
	ReductionModuloUnsigned(Unsigned const& dividend, Unsigned const& divisor) :
		divisorSize{divisor.magnitude.size()},
		normalizationShift{divisorSize * std::numeric_limits<WordType>::digits - divisor.digits()},
		divisor{divisor << normalizationShift},
		remainder{dividend.magnitude.get_allocator()},
		remainderFragment{dividend.magnitude.get_allocator()}
	{
		// Sufficient space for expansion shift result, including an extra digit appended in calculateQuotientWord() below.
		remainder.magnitude.reserve(dividend.magnitude.size() + normalizationShift / std::numeric_limits<WordType>::digits + 2);
		remainder.magnitude.assign(dividend.magnitude.begin(), dividend.magnitude.end());
		remainder <<= normalizationShift;
	}

	Unsigned computeRemainder()
	{
		if (remainder.magnitude.size() >= divisorSize) {
			iterateQuotientWords();
		}
		unnormalizeRemainder();
		return std::move(remainder);
	}

private:
	static constexpr DoubleWordType radix{DoubleWordType{1} << std::numeric_limits<WordType>::digits};

	void iterateQuotientWords()
	{
		std::size_t quotientLength = remainder.magnitude.size() - divisorSize + 1;
		for (i = quotientLength; i--;) {
			calculateQuotientWord();
			multiplyAndSubtract();
		};
	}

	void calculateQuotientWord()
	{
		remainder.magnitude.resize(i + divisorSize + 1);
		DoubleWordType trialDividend =
			(DoubleWordType{remainder.magnitude[i + divisorSize]} << std::numeric_limits<WordType>::digits) +
			remainder.magnitude[i + divisorSize - 1];
		trialQuotient = trialDividend / divisor.magnitude.back();
		trialRemainder = trialDividend % divisor.magnitude.back();
		adjustIfQuotientWordTooLarge();
		if (trialRemainder < radix) {
			adjustIfQuotientWordTooLarge();
		}
	}

	void adjustIfQuotientWordTooLarge()
	{
		if (trialQuotient == radix ||
			(trialQuotient * divisor.magnitude[divisorSize - 2]) >
				((trialRemainder << std::numeric_limits<WordType>::digits) + remainder.magnitude[i + divisorSize - 2])) {
			--trialQuotient;
			trialRemainder += divisor.magnitude.back();
		}
	}

	void multiplyAndSubtract()
	{
		remainderFragment.magnitude.assign(remainder.magnitude.begin() + i, remainder.magnitude.end());
		if (subtractAndTestNegative(divisor * trialQuotient)) {
			addBack();
		}
		remainder.magnitude.resize(i);
		remainder.magnitude.insert(
			remainder.magnitude.end(), remainderFragment.magnitude.begin(), remainderFragment.magnitude.end());
	}

	bool subtractAndTestNegative(Unsigned const& subtrahend)
	{
		DoubleWordType borrow{0};
		std::size_t j{0};
		while (j < subtrahend.magnitude.size()) {
			const DoubleWordType tmp{remainderFragment.magnitude[j] - (subtrahend.magnitude[j] + borrow)};
			remainderFragment.magnitude[j] = tmp & std::numeric_limits<WordType>::max();
			borrow = tmp >> std::numeric_limits<WordType>::digits ? 1U : 0;
			++j;
		}
		while (j < remainderFragment.magnitude.size()) {
			const DoubleWordType tmp{remainderFragment.magnitude[j] - borrow};
			remainderFragment.magnitude[j] = tmp & std::numeric_limits<WordType>::max();
			borrow = tmp >> std::numeric_limits<WordType>::digits ? 1U : 0;
			++j;
		}
		if (borrow == 0) {
			remainderFragment.trim();
		}
		return borrow != 0;
	}

	void addBack()
	{
		remainderFragment += divisor;
		remainderFragment.magnitude.pop_back();
	}

	// Revert fraction normalization
	void unnormalizeRemainder()
	{
		remainder >>= normalizationShift;
	}

	const std::size_t divisorSize;
	const std::size_t normalizationShift;
	const Unsigned divisor;
	Unsigned remainder;
	DoubleWordType trialQuotient;
	DoubleWordType trialRemainder;
	Unsigned remainderFragment;
	std::size_t i;
};

Unsigned Unsigned::operator%(Unsigned const& divisor) const
{
	if (!divisor.magnitude.empty()) {
		return divisor.magnitude.size() == 1 ? *this % divisor.magnitude.front() :
											   ReductionModuloUnsigned(*this, divisor).computeRemainder();
	} else {
		throw std::range_error("MultiPrecision::Unsigned::operator%(Unsigned const&): division by zero.");
	}
}

Unsigned& Unsigned::operator%=(Unsigned const& divisor)
{
	if (!divisor.magnitude.empty()) {
		*this = divisor.magnitude.size() == 1 ? *this % divisor.magnitude.front() :
												ReductionModuloUnsigned(*this, divisor).computeRemainder();
		return *this;
	} else {
		throw std::range_error("MultiPrecision::Unsigned::operator%=(Unsigned const&): division by zero.");
	}
}

} // namespace MultiPrecision
