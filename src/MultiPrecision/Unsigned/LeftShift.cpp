//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "MultiPrecision/Unsigned.h"
#include "MultiPrecision/WordTypes.h"

namespace MultiPrecision {

class Unsigned::LeftShift
{
public:
	LeftShift(const decltype(Unsigned::magnitude)& magnitude, decltype(Unsigned::magnitude)& result, std::size_t digits) :
		magnitude(magnitude),
		result(result),
		distanceWords(digits / std::numeric_limits<WordType>::digits),
		distanceDigits(digits % std::numeric_limits<WordType>::digits)
	{
	}

	void shiftDigits()
	{
		if (!magnitude.empty()) {
			result.resize(magnitude.size() + distanceWords + 1);
			result[result.size() - 1] = leftDigitsOf(magnitude[result.size() - distanceWords - 2]);
			for (std::size_t i{result.size() - 2}; i > distanceWords; --i) {
				result[i] = rightDigitsOf(magnitude[i - distanceWords]) | leftDigitsOf(magnitude[i - distanceWords - 1]);
			}
			result[distanceWords] = rightDigitsOf(magnitude[0]);
			for (std::size_t i{0}; i < distanceWords; ++i) {
				result[i] = 0;
			}
			if (result.back() == 0) {
				result.pop_back();
			}
		} else {
			result.clear();
		}
	}

private:
	WordType leftDigitsOf(WordType word)
	{
		return distanceDigits ? word >> (std::numeric_limits<WordType>::digits - distanceDigits) : 0;
	}

	WordType rightDigitsOf(WordType word)
	{
		return word << distanceDigits;
	}

	const decltype(Unsigned::magnitude)& magnitude;
	decltype(Unsigned::magnitude)& result;
	const std::size_t distanceWords;
	const std::size_t distanceDigits;
};

Unsigned Unsigned::operator<<(std::size_t digits) const
{
	Unsigned result{magnitude.get_allocator()};
	Unsigned::LeftShift(magnitude, result.magnitude, digits).shiftDigits();
	return result;
}

Unsigned& Unsigned::operator<<=(std::size_t digits)
{
	LeftShift(magnitude, magnitude, digits).shiftDigits();
	return *this;
}

} // namespace MultiPrecision
