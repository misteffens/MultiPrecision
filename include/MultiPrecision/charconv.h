//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#ifndef MultiPrecision_charconv_h_INCLUDED
#define MultiPrecision_charconv_h_INCLUDED

#include "MultiPrecision/Unsigned.h"
#include <charconv>

namespace MultiPrecision {

std::to_chars_result to_chars(char* first, char* last, Unsigned const& value, int base = 10) noexcept;
std::from_chars_result from_chars(char const* first, char const* last, Unsigned& value, int base = 10) noexcept;

} // namespace MultiPrecision

#endif // MultiPrecision_charconv_h_INCLUDED
