//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#ifndef MultiPrecision_Unsigned_h_INCLUDED
#define MultiPrecision_Unsigned_h_INCLUDED

#include "MultiPrecision/WordTypes.h"
#include <memory_resource>
#include <stdexcept>

namespace MultiPrecision {

class Unsigned
{
public:
	using WordType = MultiPrecision::WordType;

	struct DivisionByUnsignedResult;
	struct DivisionByWordTypeResult;

	Unsigned(std::pmr::polymorphic_allocator<WordType> const& alloc) : magnitude{alloc}
	{
	}

	template<typename T, typename std::enable_if_t<std::is_unsigned_v<T> || std::is_same_v<T, DoubleWordType>, bool> = true>
	Unsigned(T n, std::pmr::polymorphic_allocator<WordType> const& alloc) : magnitude{alloc}
	{
		*this = n;
	}

	template<typename T, typename std::enable_if_t<std::is_unsigned_v<T> || std::is_same_v<T, DoubleWordType>, bool> = true>
	Unsigned& operator=(T n)
	{
		magnitude.clear();
		if (n != 0) {
			if constexpr (sizeof(T) > sizeof(WordType)) {
				magnitude.reserve(sizeof(T) / sizeof(WordType));
				do {
					magnitude.push_back(WordType(n));
					n >>= std::numeric_limits<WordType>::digits;
				} while (n != 0);
			} else {
				magnitude.push_back(WordType(n));
			}
		}
		return *this;
	}

	template<typename InputIterator>
	Unsigned(
		InputIterator leastSignificantWord,
		InputIterator mostSignificantWord,
		std::pmr::polymorphic_allocator<WordType> const& alloc) :
		magnitude{leastSignificantWord, mostSignificantWord, alloc}
	{
		trim();
	}

	template<typename T, typename std::enable_if_t<std::is_unsigned_v<T> || std::is_same_v<T, DoubleWordType>, bool> = true>
	explicit operator T() const
	{
		if (magnitude.size() * sizeof(WordType) <= sizeof(T)) {
			T result{0};
			for (decltype(magnitude)::const_reverse_iterator i{magnitude.rbegin()}; i != magnitude.rend(); ++i) {
				result = (result << std::numeric_limits<WordType>::digits) | *i;
			}
			return result;
		} else {
			throw std::range_error("MultiPrecision::Unsigned::operator T(): Magnitude exceeds size of T.");
		}
	}

	std::string toHexString() const;

	bool operator==(Unsigned const& other) const noexcept;
	bool operator!=(Unsigned const& other) const noexcept
	{
		return !(*this == other);
	}
	bool operator<(Unsigned const& other) const noexcept;
	bool operator<=(Unsigned const& other) const noexcept
	{
		return !(other < *this);
	}
	bool operator>(Unsigned const& other) const noexcept
	{
		return other < *this;
	}
	bool operator>=(Unsigned const& other) const noexcept
	{
		return !(*this < other);
	}
	bool operator==(WordType other) const noexcept;
	bool operator!=(WordType other) const noexcept
	{
		return !(*this == other);
	}
	bool operator<(WordType other) const noexcept;
	bool operator<=(WordType other) const noexcept
	{
		return !(*this > other);
	}
	bool operator>(WordType other) const noexcept;
	bool operator>=(WordType other) const noexcept
	{
		return !(*this < other);
	}
	Unsigned operator+(Unsigned const& addend) const;
	Unsigned& operator+=(Unsigned const& addend);
	Unsigned operator+(WordType addend) const;
	Unsigned& operator+=(WordType addend);
	Unsigned operator-(Unsigned const& subtrahend) const;
	Unsigned& operator-=(Unsigned const& subtrahend);
	Unsigned operator-(WordType subtrahend) const;
	Unsigned& operator-=(WordType subtrahend);
	Unsigned operator*(Unsigned const& multiplier) const;
	Unsigned& operator*=(Unsigned const& multiplier);
	Unsigned operator*(WordType multiplier) const;
	Unsigned& operator*=(WordType multiplier);
	Unsigned operator/(Unsigned const& divisor) const;
	Unsigned& operator/=(Unsigned const& divisor);
	Unsigned operator/(WordType divisor) const;
	Unsigned& operator/=(WordType divisor);
	Unsigned operator%(Unsigned const& divisor) const;
	Unsigned& operator%=(Unsigned const& divisor);
	Unsigned operator%(WordType divisor) const;
	Unsigned& operator%=(WordType divisor);
	DivisionByUnsignedResult div(Unsigned const& divisor) const;
	DivisionByWordTypeResult div(WordType divisor) const;

	std::size_t digits() const noexcept;
	Unsigned operator<<(std::size_t digits) const;
	Unsigned& operator<<=(std::size_t digits);
	Unsigned operator>>(std::size_t digits) const;
	Unsigned& operator>>=(std::size_t digits);

private:
	class AdditionOfUnsigned;
	class AdditionOfWordType;
	class SubtractionOfUnsigned;
	class SubtractionOfWordType;
	class MultiplicationByUnsigned;
	class MultiplicationByWordType;
	class DivisionByUnsigned;
	class DivisionByWordType;
	class ReductionModuloUnsigned;
	class ReductionModuloWordType;
	class LeftShift;
	class RightShift;

	std::pmr::vector<WordType> magnitude;

	void trim() noexcept;
};

struct Unsigned::DivisionByWordTypeResult
{
	Unsigned quotient;
	Unsigned::WordType remainder;
};

struct Unsigned::DivisionByUnsignedResult
{
	DivisionByUnsignedResult(Unsigned&& quotient, Unsigned&& remainder) :
		quotient{std::move(quotient)}, remainder{std::move(remainder)}
	{
	}

	DivisionByUnsignedResult(DivisionByWordTypeResult&& other) :
		quotient{std::move(other.quotient)}, remainder{other.remainder, other.quotient.magnitude.get_allocator()}
	{
	}

	Unsigned quotient;
	Unsigned remainder;
};

std::ostream& operator<<(std::ostream& os, Unsigned const& n);

} // namespace MultiPrecision

#endif // MultiPrecision_Unsigned_h_INCLUDED
