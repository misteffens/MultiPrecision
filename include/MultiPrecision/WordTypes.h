// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#ifndef MultiPrecision_WordTypes_h_INCLUDED
#define MultiPrecision_WordTypes_h_INCLUDED

#include <cstdint>
#include <type_traits>

namespace MultiPrecision {

using WordType = std::uint64_t;
using DoubleWordType = unsigned __int128;

static_assert(std::is_unsigned<WordType>::value);
static_assert(sizeof(DoubleWordType) == 2 * sizeof(WordType));

} // namespace MultiPrecision

#endif // MultiPrecision_WordTypes_h_INCLUDED
